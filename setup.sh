# setup the environment
if [ "$JAVA_HOME" != "" ] ; then
    echo "Using JAVA_HOME:       $JAVA_HOME"
else
    echo "ERROR: no JAVA_HOME variable is set. Exiting process..."
    exit 1
fi

while read -r env_var; do
    export "$env_var"
done < cerndb-mwod-api.list
