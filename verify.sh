#!/bin/sh

# setup the environment: JAVA_HOME and enviroment variables
./setup.sh
return_code=$?
if [ $return_code != 0 ] ; then
   echo "ERROR: setup.sh returns $return_code"
   exit 1
else
   echo "ENVIROMENT SETUP OK"
fi

# Remove target folder (binaries)
/home/lurodrig/development/ci/apache-maven-3.3.9/bin/mvn clean 

# Compile: create target folder 
/home/lurodrig/development/ci/apache-maven-3.3.9/bin/mvn compile

# Package: create the .war inside the target folder 
/home/lurodrig/development/ci/apache-maven-3.3.9/bin/mvn package -DskipUTs=true

# Build the container using the Dockerfile in the project root (current directory)
# This build will add the binaries (.war) and the scripts (vegas commands)
docker build -t gitlab-registry.cern.ch/db/cerndb-docker-registry:java8_maven3_mwod_rest_api_dev . -f Dockerfile.dev --force-rm

# Run the integration tests (mvn verify) skipping the unit ones
# Run the container exposing the HTTP (8080) and debug port (4567)
docker run --env-file cerndb-mwod-api.list --user 1001 -p 127.0.0.1:8080:8080 -p 127.0.0.1:4567:4567 -i -t gitlab-registry.cern.ch/db/cerndb-docker-registry:java8_maven3_mwod_rest_api_dev
