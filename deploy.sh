#!/bin/sh

check_return_code () {
code=$1
step=$2
if [ $code != 0 ] ; then
   echo "ERROR: $step returns $code"
   exit 1
else
   echo "$step OK"
fi
}

# Build the docker image and push it to the registry
./build_and_push.sh
check_return_code $? "BUILD_AND_PUSH"

# Update the environment variables in openshift
cat cerndb-mwod-api.list | oc set env -e - dc/cerndb-mwod-api
check_return_code $? "SET_OPENSHIFT_DEPLOYMENT_ENV"

# Take the image from the registry and put in openshift. It will trigger a new deployment
oc import-image gitlab-registry.cern.ch/db/cerndb-mwod-api:java8_maven3_mwod_rest_api
check_return_code $? "IMPORT_IMAGE"


