#!/bin/bash
entity=$1
ldap_host=$2
ldap_port=$3

# kkinit takes above location by default (/ORA/dbs01/syscontrol/local/passwd/$user@CERN.CH.keytab)
/ORA/dbs01/syscontrol/bin/kkinit -ku $SERVICE_ACCOUNT_NAME vegas-cli delete $entity --server $ldap_host --port $ldap_port
exit $?
