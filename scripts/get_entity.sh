#!/bin/bash
sc_entity=$1

# kkinit takes above location by default (/ORA/dbs01/syscontrol/local/passwd/$user@CERN.CH.keytab)
/ORA/dbs01/syscontrol/bin/kkinit -ku $SERVICE_ACCOUNT_NAME vegas-cli json "entities[?sc_entity=='$sc_entity']"
exit $?
