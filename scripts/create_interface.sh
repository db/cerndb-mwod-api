#!/bin/sh
base_dn=$1
json_file=$2
ldap_host=$3
ldap_port=$4

# kkinit takes above location by default (/ORA/dbs01/syscontrol/local/passwd/$user@CERN.CH.keytab)
/ORA/dbs01/syscontrol/bin/kkinit -ku $SERVICE_ACCOUNT_NAME vegas-cli add --force --base_dn $base_dn $json_file --server $ldap_host --port $ldap_port
