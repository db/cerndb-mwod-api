#!/bin/bash
sc_entity=$1
sc_interface_id=$2

# kkinit takes above location by default (/ORA/dbs01/syscontrol/local/passwd/$user@CERN.CH.keytab)
/ORA/dbs01/syscontrol/bin/kkinit -ku $SERVICE_ACCOUNT_NAME vegas-cli json "entities[?sc_entity=='$sc_entity'].interfaces[] | [?sc_interface_id=='$sc_interface_id']"
exit $?
