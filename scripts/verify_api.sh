#!/bin/sh

$VEGAS_SCRIPTS_HOME/setup_vegas.sh

#mvn clean compile package -DskipUTs=true
mvn verify -Dmaven.failsafe.debug="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=4567 -Xnoagent -Djava.compiler=NONE"
