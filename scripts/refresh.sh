#!/bin/bash

# kkinit takes above location by default (/ORA/dbs01/syscontrol/local/passwd/$user@CERN.CH.keytab)
/ORA/dbs01/syscontrol/bin/kkinit -ku $SERVICE_ACCOUNT_NAME vegas-cli refresh
exit $?
