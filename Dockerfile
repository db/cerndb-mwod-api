FROM gitlab-registry.cern.ch/db/cerndb-docker-registry:java8_maven3

MAINTAINER Luis Rodriguez Fernandez <luis.rodriguez.fernandez@cern.ch>

# Make the project name as the workdiri ("sets the working directory for any RUN, CMD, ENTRYPOINT, COPY and ADD instructions that follow it")
WORKDIR /cerndb-mwod-api

# maven descriptor
ADD pom.xml pom.xml

# Binaries (.war)
ADD target target

# Vegas scripts
ADD scripts scripts

# SSL configuration: keystore
ADD keystore.jks keystore.jks

# Change the ownership of all the above folders to a random user
RUN chown -R 1001:0 /cerndb-mwod-api
#Set permissions to any user
RUN chmod -R 777 /cerndb-mwod-api

# Change the ownership of the syscontrol folders and files
RUN chown -R 1001:0 /ORA/dbs01/syscontrol
#Set permissions to any user
RUN chmod -R 777 /ORA/dbs01/syscontrol

# Spring-boot embedded tomcat listen by default here (plain HTTP)
EXPOSE 8080
# HTTPS port
EXPOSE 8443
# Verify script exposes the JAVA debug port 4567
EXPOSE 4567

# Run the application
CMD scripts/run_api.sh 
