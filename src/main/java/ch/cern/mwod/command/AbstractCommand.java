package ch.cern.mwod.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import ch.cern.mwod.config.DefaultsConfig;

public class AbstractCommand {

	boolean silent;
	@Autowired
	private DefaultsConfig defaultsHelper;
	@Value("${DEFAULTS_DB_USERNAME:xxx}")
	private String username;
	@Value("${LDAP_HOST:xxx}")
	private String ldapHost;
	@Value("${LDAP_PORT:xxx}")
	private String ldapPort;
	@Value("${VEGAS_SCRIPTS_HOME:xxx}")
	private String VEGAS_SCRIPTS_HOME;
	private String commandLine;
	
	public void setVegasScriptHomeInCommandLine(){
		setCommandLine(getCommandLine().replace("$VEGAS_SCRIPTS_HOME", VEGAS_SCRIPTS_HOME));
	}
	
	public void setUserInCommandline() {
		setCommandLine(getCommandLine().replace("$user", getUsername()));
	}

	public void setLdapPortHostInCommandLine() {
		setCommandLine(getCommandLine().replace("$ldap_host", getLdapHost()));
		setCommandLine(getCommandLine().replace("$ldap_port", getLdapPort()));
	}

	public boolean isSilent() {
		return silent;
	}

	public void setSilent(boolean silent) {
		this.silent = silent;
	}

	public DefaultsConfig getDefaultsHelper() {
		return defaultsHelper;
	}

	public void setDefaultsHelper(DefaultsConfig defaultsHelper) {
		this.defaultsHelper = defaultsHelper;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLdapHost() {
		return ldapHost;
	}

	public void setLdapHost(String ldapHost) {
		this.ldapHost = ldapHost;
	}

	public String getLdapPort() {
		return ldapPort;
	}

	public void setLdapPort(String ldapPort) {
		this.ldapPort = ldapPort;
	}

	public String getCommandLine() {
		return commandLine;
	}

	public void setCommandLine(String commandLine) {
		this.commandLine = commandLine;
	}

	@Override
	public String toString() {
		return "AbstractCommand [silent=" + silent + ", commandLine=" + commandLine + "]";
	}
}
