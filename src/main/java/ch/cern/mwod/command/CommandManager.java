package ch.cern.mwod.command;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.cern.mwod.commandline.CommandOutput;
import ch.cern.mwod.commandline.ScriptRunner;

@Service
public class CommandManager extends AbstractCommand{
	
	@Value("${vegas_cli_refresh:xxx}")
	private String vegas_cli_refresh;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public synchronized CommandOutput execute(Command command) throws Exception{
		CommandOutput output = null;
		try {
			setCommandLine(vegas_cli_refresh);
			setUserInCommandline();
			setVegasScriptHomeInCommandLine();
			setLdapPortHostInCommandLine();
			ScriptRunner.run(getCommandLine(), 1, TimeUnit.MINUTES, false);
			logger.debug(getCommandLine());
			output = (CommandOutput) command.execute();
			logger.debug("RESULT: " + output.getResult());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return output;
	}
}
