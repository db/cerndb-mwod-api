package ch.cern.mwod.command;

public interface Command {

	public Object execute() throws Exception;
}
