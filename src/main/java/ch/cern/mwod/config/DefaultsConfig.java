package ch.cern.mwod.config;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ch.cern.mwod.commandline.CommandOutput;
import ch.cern.mwod.commandline.ScriptRunner;
import ch.cern.mwod.http.Client;
import ch.cern.mwod.model.Defaults;
import ch.cern.mwod.model.Item;

@Component
public class DefaultsConfig {

	private Client client = new Client();
	private Defaults defaults;

	@Autowired
	public DefaultsConfig(@Value("${DEFAULTS_DB_URL:xxx}") String defaultsDbUri,
			@Value("${DEFAULTS_DB_USERNAME:xxx}") String username,
			@Value("${GET_PASSWD_SCRIPT:xxx}") String getPasswdScript,
			@Value("${DEFAULTS_DB_PASSWORD:xxx}") String password,
			@Value("${PRINT_ENVIROMENT:false}") String printEnviroment,
			@Value("${USE_DEFAULTS_DB:false}") String useDefaultsDb) {
		if (password == null || password.isEmpty()) {
			password = getPassword(username, getPasswdScript);
		}
		if (printEnviroment != null && Boolean.valueOf(printEnviroment)) {
			Map<String, String> items = System.getenv();
			items.forEach((k, v) -> System.out.println("Item : " + k + " Value : " + v));
		}
		if (useDefaultsDb != null && Boolean.valueOf(useDefaultsDb)) {
			this.defaults = (Defaults) client.get(defaultsDbUri, username, password);
		}
	}

	private String getPassword(String username, String getPasswdScript) {
		String password = null;
		try {
			getPasswdScript = getPasswdScript + " password_user_" + username;
			CommandOutput commandOutput = ScriptRunner.run(getPasswdScript, 1, TimeUnit.MINUTES, true);
			if (commandOutput.getResult() == 0) {
				password = commandOutput.getBufferedOutput().toString().trim();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return password;
	}

	public Item getDefaultByName(String name) {
		List<Item> items = this.defaults.getItems();
		for (Item item : items) {
			if (item.getName().equals(name)) {
				return item;
			}
		}
		return null;
	}

	public Defaults getDefaults() {
		return defaults;
	}
}
