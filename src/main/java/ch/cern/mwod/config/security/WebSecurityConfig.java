package ch.cern.mwod.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Value("${USER_SEARCH_BASE:xxx}")
	private String userSearchBase;
	@Value("${USER_DN_PATTERNS:xxx}")
	private String userDnPatterns;
	@Value("${GROUP_SEARCH_BASE:xxx}")
	private String groupSearchBase;
	@Value("${GROUP_SEARCH_FILTER:xxx}")
	private String groupSearchFilter;
	@Value("${GROUP_ROLE_ATTRIBUTE:xxx}")
	private String groupRoleAttribute;
	@Value("${MANAGER_DN:xxx}")
	private String managerDn;
	@Value("${MANAGER_PASSWORD:xxx}")
	private String managerPassword;
	@Value("${LDAP_AUTHENTICATOR_URL:xxx}")
	private String url;
	@Value("${ACCESS:xxx}")
	private String access;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.csrf().disable()
			.authorizeRequests()
			.antMatchers("/api/**")
			.access(access)
			.and()
			.httpBasic();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.ldapAuthentication()
		        .userSearchBase(userSearchBase)
				.userDnPatterns(userDnPatterns)
				.groupSearchBase(groupSearchBase)
				.groupSearchFilter(groupSearchFilter)
				.groupRoleAttribute(groupRoleAttribute)
				.contextSource()
				.managerDn(managerDn)
				.managerPassword(managerPassword)
				.url(url);
	}
}
