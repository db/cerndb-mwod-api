package ch.cern.mwod;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import ch.cern.mwod.config.security.SsoFilter;

@SpringBootApplication
public class MwodAPI extends SpringBootServletInitializer {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(MwodAPI.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(MwodAPI.class);
	}
	
    @Bean
    public ServletRegistrationBean ui() {
        DispatcherServlet dispatcherServlet = new DispatcherServlet();
        ApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
        dispatcherServlet.setApplicationContext(applicationContext);
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(dispatcherServlet, "/ui/*");
        servletRegistrationBean.setName("ui");
        return servletRegistrationBean;
    }
    
    @Bean
    public ServletRegistrationBean api() {
        DispatcherServlet dispatcherServlet = new DispatcherServlet();
        ApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
        dispatcherServlet.setApplicationContext(applicationContext);
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(dispatcherServlet, "/api/*");
        servletRegistrationBean.setName("api");
        return servletRegistrationBean;
    }
    
    @Bean
    public FilterRegistrationBean ssoFilterBean(){
    	FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
    	filterRegistrationBean.setName("ssoFilter");
    	SsoFilter ssoFilter = new SsoFilter();
    	filterRegistrationBean.setFilter(ssoFilter);
    	filterRegistrationBean.setOrder(FilterRegistrationBean.LOWEST_PRECEDENCE);
    	Collection<String> urlPatterns = new ArrayList<String>();
    	urlPatterns.add("/ui/*");
		filterRegistrationBean.setUrlPatterns(urlPatterns);
    	return filterRegistrationBean;
    }

}
