package ch.cern.mwod.http;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import ch.cern.mwod.commandline.CommandOutput;

@Component
public class HttpResponseHelper {

	public ResponseEntity<Object> buildResponse(HttpStatus httpStatus, Object object,
			CommandOutput commandOutput) {
		if (commandOutput.getResult() == 0) {
			return ResponseEntity.status(httpStatus).body(object);
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(commandOutput.getBufferedOutput());
		}
	}

}
