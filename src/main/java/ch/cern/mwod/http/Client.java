package ch.cern.mwod.http;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import ch.cern.mwod.model.Defaults;

public class Client {

	private HttpClientConfig httpClientConfig = new HttpClientConfig();
	
	public Object get(String uri, String username, String password) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Defaults> responseEntity = restTemplate.exchange(uri, HttpMethod.GET,
				httpClientConfig.basicAuth(username, password), Defaults.class);
		return responseEntity.getBody();
	}

}
