package ch.cern.mwod.http;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

public class HttpClientConfig {
	
	private final static String BASIC = "Basic ";
	
	public HttpEntity<?> basicAuth(String username, String password){
		 String plainCreds = username + ":" + password;
		 byte[] plainCredsBytes = plainCreds.getBytes();
		 byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		 String base64Creds = new String(base64CredsBytes);
		 HttpHeaders headers = new HttpHeaders();
		 headers.add(HttpHeaders.AUTHORIZATION, BASIC + base64Creds);
		 HttpEntity<String> httpEntity = new HttpEntity<String>(headers);
		 return httpEntity;
	}	
}
