package ch.cern.mwod.tomcats;

import org.springframework.stereotype.Service;

import ch.cern.mwod.command.CommandManager;
import ch.cern.mwod.commandline.CommandOutput;
import ch.cern.mwod.json.JsonManager;
import ch.cern.mwod.model.Tomcat;

@Service
public class ListTomcatService {

	private CommandManager commandManager;
	private ListTomcatCommand command;

	public ListTomcatService(CommandManager commandManager, ListTomcatCommand command) {
		super();
		this.commandManager = commandManager;
		this.command = command;
	}

	public Tomcat[] list(boolean silent) throws Exception {
		command.setSilent(silent);
		CommandOutput commandOutput = this.commandManager.execute(command);
		Tomcat[] result = null;
		if (commandOutput != null) {
			result = (Tomcat[]) JsonManager.deserializeJson(commandOutput.getBufferedOutput().toString(),
					Tomcat[].class);
		}
		return result;
	}
}
