package ch.cern.mwod.tomcats;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.cern.mwod.command.AbstractCommand;
import ch.cern.mwod.command.Command;
import ch.cern.mwod.commandline.ScriptRunner;

@Service
public class AddTomcatCommand extends AbstractCommand implements Command {

	private String json_file;
	@Value("${add_tomcat_command:xxx}")
	private String add_tomcat_command;

	@Override
	public Object execute() throws Exception {
		setCommandLine(add_tomcat_command);
		setUserInCommandline();
		setVegasScriptHomeInCommandLine();
		setCommandLine(getCommandLine().replace("$json_file", json_file));
		setLdapPortHostInCommandLine();
		return ScriptRunner.run(getCommandLine(), 1, TimeUnit.MINUTES, isSilent());
	}

	public String getJson_file() {
		return json_file;
	}

	public void setJson_file(String json_file) {
		this.json_file = json_file;
	}
}
