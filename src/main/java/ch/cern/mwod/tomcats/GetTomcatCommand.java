package ch.cern.mwod.tomcats;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.cern.mwod.command.AbstractCommand;
import ch.cern.mwod.command.Command;
import ch.cern.mwod.commandline.ScriptRunner;

@Service
public class GetTomcatCommand extends AbstractCommand implements Command {

	private String sc_entity;
	@Value("${get_tomcat_command:xxx}")
	private String get_tomcat_command;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public Object execute() throws Exception {
		setCommandLine(get_tomcat_command);
		setVegasScriptHomeInCommandLine();
		setCommandLine(getCommandLine().replace("$sc_entity", sc_entity));
		logger.debug(this.toString());
		return ScriptRunner.run(getCommandLine(), 1, TimeUnit.MINUTES, isSilent());
	}

	public String getSc_entity() {
		return sc_entity;
	}

	public void setSc_entity(String sc_entity) {
		this.sc_entity = sc_entity;
	}
}
