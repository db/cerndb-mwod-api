package ch.cern.mwod.tomcats;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.cern.mwod.command.CommandManager;
import ch.cern.mwod.commandline.CommandOutput;

@Service
public class DeleteTomcatService {

	private CommandManager commandManager;
	private DeleteTomcatCommand command;
	@Value("${TOMCATS_BASE_DN:xxx}")
	private String tomcatsBaseDN;

	public DeleteTomcatService(CommandManager commandManager, DeleteTomcatCommand command) {
		super();
		this.commandManager = commandManager;
		this.command = command;
	}

	public String delete(String sc_entity, boolean silent) throws Exception {
		String result = null;
		if (sc_entity != null) {
			command.setSilent(silent);
			command.setSc_entity(sc_entity);
			CommandOutput commandOutput = this.commandManager.execute(command);
			if (commandOutput != null) {
				if (commandOutput.getResult() == 0) {
					result = tomcatsBaseDN.replace("$sc_entity", sc_entity);
				}
			}
		}
		return result;
	}
}
