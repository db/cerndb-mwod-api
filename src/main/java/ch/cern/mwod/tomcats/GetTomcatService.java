package ch.cern.mwod.tomcats;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.cern.mwod.command.CommandManager;
import ch.cern.mwod.commandline.CommandOutput;
import ch.cern.mwod.json.JsonManager;
import ch.cern.mwod.model.Tomcat;

@Service
public class GetTomcatService {

	private CommandManager commandManager;
	private GetTomcatCommand command;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public GetTomcatService(CommandManager commandManager, GetTomcatCommand command) {
		super();
		this.commandManager = commandManager;
		this.command = command;
	}

	public Tomcat get(String sc_entity, boolean silent) throws Exception {
		logger.debug(sc_entity + " " + silent); 
		command.setSc_entity(sc_entity);
		command.setSilent(silent);
		CommandOutput commandOutput = this.commandManager.execute(command);
		Tomcat result = null;
		if (commandOutput != null) {
			logger.debug(commandOutput.toString());
			Tomcat[] tomcats = (Tomcat[]) JsonManager.deserializeJson(commandOutput.getBufferedOutput().toString(),
					Tomcat[].class);
			if (tomcats != null && tomcats.length > 0) {
				result = tomcats[0];
				logger.debug(result.toString());
			}
		}
		return result;
	}
}
