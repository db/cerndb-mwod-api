package ch.cern.mwod.tomcats;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.cern.mwod.commons.CoreCommons;
import ch.cern.mwod.model.Tomcat;

@RestController
public class TomcatController {

	@Autowired
	private GetTomcatService getTomcatService;
	@Autowired
	private AddTomcatService addTomcatService;
	@Autowired
	private DeleteTomcatService deleteTomcatService;
	@Autowired
	private ListTomcatService listTomcatService;

	@GetMapping("/entities/tomcats/{sc_entity}")
	public ResponseEntity<Object> getTomcat(@PathVariable String sc_entity,
			@RequestParam(required = false, defaultValue = "true") boolean silent) {
		Tomcat tomcat = null;
		ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(sc_entity + " " + (HttpStatus.NOT_FOUND.getReasonPhrase()));
		try {
			tomcat = this.getTomcatService.get(sc_entity, silent);
			if (tomcat != null) {
				responseEntity = ResponseEntity.status(HttpStatus.OK).body(tomcat);
			}
		} catch (Exception e) {
			responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
			// TODO
			e.printStackTrace();
		}
		return responseEntity;
	}

	@PostMapping("/entities/tomcats")
	public ResponseEntity<String> addTomcat(@RequestBody Tomcat tomcat,
			@RequestParam(required = false, defaultValue = "true") boolean silent) {
		ResponseEntity<String> responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
		try {
			String tomcatDN = this.addTomcatService.add(tomcat, silent);
			if (tomcatDN != null) {
				responseEntity = ResponseEntity.status(HttpStatus.CREATED)
						.body(tomcatDN + " " + HttpStatus.CREATED.getReasonPhrase());
			}
		} catch (Exception e) {
			responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
			// TODO
			e.printStackTrace();
		}
		return responseEntity;
	}

	@DeleteMapping("/entities/tomcats/{sc_entity}")
	public ResponseEntity<String> deleteTomcat(@PathVariable String sc_entity,
			@RequestParam(required = false, defaultValue = "true") boolean silent) {
		ResponseEntity<String> responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(sc_entity + " " + (HttpStatus.NOT_FOUND.getReasonPhrase()));
		try {
			String tomcatDN = this.deleteTomcatService.delete(sc_entity, silent);
			if (tomcatDN != null) {
				responseEntity = ResponseEntity.status(HttpStatus.OK).body(tomcatDN + " " + CoreCommons.DELETED);
			}
		} catch (Exception e) {
			responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
			// TODO
			e.printStackTrace();
		}
		return responseEntity;
	}

	@GetMapping("/entities/tomcats")
	public ResponseEntity<Object> listTomcats(@RequestParam(required = false, defaultValue = "true") boolean silent) {
		Tomcat[] tomcats = null;
		ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
				.body(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
		try {
			tomcats = this.listTomcatService.list(silent);
			if (tomcats != null && tomcats.length > 0) {
				responseEntity = ResponseEntity.status(HttpStatus.OK).body(tomcats);
			}
		} catch (Exception e) {
			responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
			// TODO
			e.printStackTrace();
		}
		return responseEntity;
	}

}
