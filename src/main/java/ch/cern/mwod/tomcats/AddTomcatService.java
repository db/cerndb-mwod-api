package ch.cern.mwod.tomcats;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.cern.mwod.command.CommandManager;
import ch.cern.mwod.commandline.CommandOutput;
import ch.cern.mwod.commons.CoreCommons;
import ch.cern.mwod.json.JsonManager;
import ch.cern.mwod.model.Tomcat;

@Service
public class AddTomcatService {

	private CommandManager commandManager;
	private AddTomcatCommand command;
	@Value("${TOMCATS_BASE_DN:xxx}")
	private String tomcatsBaseDN;

	public AddTomcatService(CommandManager commandManager, AddTomcatCommand command) {
		super();
		this.commandManager = commandManager;
		this.command = command;
	}

	public String add(Tomcat tomcat, boolean silent) throws Exception {
		String result = null;
		if (tomcat != null) {
			Path dir = Paths.get(CoreCommons.TEMP_DIRECTORY);
			String json_file = Files.createTempFile(dir, "tomcat", ".json").toString();
			JsonManager.serializeJson(tomcat, json_file);
			command.setSilent(silent);
			command.setJson_file(json_file);
			CommandOutput commandOutput = this.commandManager.execute(command);
			if (commandOutput != null) {
				if (commandOutput.getResult() == 0) {
					result = tomcatsBaseDN.replace("$sc_entity", tomcat.getScEntity());
				}
			}
		}
		return result;
	}
}
