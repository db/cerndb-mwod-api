package ch.cern.mwod.tomcats;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.cern.mwod.command.AbstractCommand;
import ch.cern.mwod.command.Command;
import ch.cern.mwod.commandline.ScriptRunner;

@Service
public class ListTomcatCommand extends AbstractCommand implements Command {
	
	@Value("${SC_SUBCATEGORY:xxx}") 
	private String sc_subcategory;
	@Value("${list_entities_by_subcategory_command:xxx}")
	private String list_entities_by_subcategory_command;
	
	@Override
	public Object execute() throws Exception {
		setCommandLine(list_entities_by_subcategory_command);
		setVegasScriptHomeInCommandLine();
		setCommandLine(getCommandLine().replace("$sc_subcategory", sc_subcategory));
		return ScriptRunner.run(getCommandLine(), 1, TimeUnit.MINUTES, isSilent());
	}
}
