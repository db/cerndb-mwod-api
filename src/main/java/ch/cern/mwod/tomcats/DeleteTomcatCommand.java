package ch.cern.mwod.tomcats;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.cern.mwod.command.AbstractCommand;
import ch.cern.mwod.command.Command;
import ch.cern.mwod.commandline.ScriptRunner;
import ch.cern.mwod.config.DefaultsConfig;

@Service
public class DeleteTomcatCommand extends AbstractCommand implements Command {

	private String sc_entity;
	@Value("${delete_tomcat_command:xxx}")
	private String delete_tomcat_command;

	@Override
	public Object execute() throws Exception {
		setCommandLine(delete_tomcat_command);
		setUserInCommandline();
		setVegasScriptHomeInCommandLine();
		setCommandLine(getCommandLine().replace("$sc_entity", sc_entity));
		setLdapPortHostInCommandLine();
		return ScriptRunner.run(getCommandLine(), 1, TimeUnit.MINUTES, isSilent());
	}

	public String getSc_entity() {
		return sc_entity;
	}

	public void setSc_entity(String sc_entity) {
		this.sc_entity = sc_entity;
	}

}
