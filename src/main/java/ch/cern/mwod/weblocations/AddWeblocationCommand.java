package ch.cern.mwod.weblocations;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.cern.mwod.command.AbstractCommand;
import ch.cern.mwod.command.Command;
import ch.cern.mwod.commandline.ScriptRunner;

@Service
public class AddWeblocationCommand extends AbstractCommand implements Command {

	private String sc_entity;
	private String json_file;
	private String sc_interface_id;
	@Value("${WEBLOCATIONS_BASE_DN:xxx}")
	private String weblocationsBaseDN;
	@Value("${add_interface_command:xxx}")
	private String add_weblocation_command;

	@Override
	public Object execute() throws Exception {
		this.weblocationsBaseDN = this.weblocationsBaseDN.replace("$sc_entity", this.sc_entity)
				.replace("$sc_interface_id", this.sc_interface_id);
		setCommandLine(add_weblocation_command);
		setVegasScriptHomeInCommandLine();
		setCommandLine(getCommandLine().replace("$base_dn", this.weblocationsBaseDN));
		setCommandLine(getCommandLine().replace("$json_file", json_file));
		setLdapPortHostInCommandLine();
		return ScriptRunner.run(getCommandLine(), 1, TimeUnit.MINUTES, isSilent());
	}

	public String getSc_entity() {
		return sc_entity;
	}

	public void setSc_entity(String sc_entity) {
		this.sc_entity = sc_entity;
	}

	public String getSc_interface_id() {
		return sc_interface_id;
	}

	public void setSc_interface_id(String sc_interface_id) {
		this.sc_interface_id = sc_interface_id;
	}

	public String getJson_file() {
		return json_file;
	}

	public void setJson_file(String json_file) {
		this.json_file = json_file;
	}
}
