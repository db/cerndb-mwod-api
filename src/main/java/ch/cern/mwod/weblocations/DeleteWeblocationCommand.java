package ch.cern.mwod.weblocations;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import ch.cern.mwod.command.AbstractCommand;
import ch.cern.mwod.command.Command;
import ch.cern.mwod.commandline.CommandOutput;
import ch.cern.mwod.commandline.ScriptRunner;
import ch.cern.mwod.commons.CoreCommons;
import ch.cern.mwod.interfaces.Interface;
import ch.cern.mwod.json.JsonManager;

@Service
public class DeleteWeblocationCommand extends AbstractCommand implements Command {

	private String sc_entity;
	private String sc_interface_id;
	private String sc_web_loc_id;
	private ListOfWeblocations weblocations;
	private Weblocation weblocationToDelete;
	private String baseDN;
	@Value("${add_weblocation_command:xxx}")
	private String add_weblocation_command;
	@Value("${get_weblocation_command:xxx}")
	private String get_weblocation_command;
	@Value("${get_weblocations_command:xxx}")
	private String get_weblocations_command;

	@Override
	public Object execute() throws Exception {
		getWeblocationToDelete();
		getAllWeblocations();
		removeWeblocationFromList();
		return deleteWeblocation();
	}

	private CommandOutput deleteWeblocation()
			throws JsonGenerationException, JsonMappingException, IOException, Exception {
		Path dir = Paths.get(CoreCommons.TEMP_DIRECTORY);
		String jsonFilePathname = Files
				.createTempFile(dir, Interface[].class.getCanonicalName(), CoreCommons.JSON_FILE_SUFFIX).toString();
		JsonManager.serializeJson(this.weblocations, jsonFilePathname);
		this.baseDN = "SC-INTERFACE-ID=" + sc_interface_id + ",SC-CATEGORY=interfaces,SC-ENTITY=" + sc_entity
				+ ",SC-CATEGORY=entities,ou=syscontrol,dc=cern,dc=ch";		 
		setCommandLine(add_weblocation_command);
		setCommandLine(getCommandLine().replace("$base_dn", this.baseDN));
		setCommandLine(getCommandLine().replace("$json_file", jsonFilePathname));
		setVegasScriptHomeInCommandLine();
		setLdapPortHostInCommandLine();
		CommandOutput commandOutput = ScriptRunner.run(getCommandLine(), 1, TimeUnit.MINUTES, isSilent());
		return commandOutput;
	}

	private void removeWeblocationFromList() throws Exception {
		this.weblocations.removeWeblocation(this.weblocationToDelete);
	}

	private void getWeblocationToDelete() throws Exception {
		setCommandLine(get_weblocation_command);
		setCommandLine(getCommandLine().replace("$sc_entity", this.sc_entity));
		setCommandLine(getCommandLine().replace("$sc_web_loc_id", this.sc_web_loc_id));
		setVegasScriptHomeInCommandLine();
		CommandOutput commandOutput = ScriptRunner.run(getCommandLine(), 1, TimeUnit.MINUTES, isSilent());
		if (commandOutput.getResult() == 0) {
			this.weblocationToDelete = ((Weblocation[]) JsonManager
					.deserializeJson(commandOutput.getBufferedOutput().toString(), Weblocation[].class))[0];
		} else {
			throw new Exception("ERROR getting the weblocation " + sc_web_loc_id + " to delete from " + this.sc_entity
					+ "." + this.sc_interface_id);
		}
	}

	private void getAllWeblocations() throws Exception {
		setCommandLine(get_weblocations_command);
		setCommandLine(getCommandLine().replace("$sc_entity", this.sc_entity));
		setCommandLine(getCommandLine().replace("$sc_interface_id", this.sc_interface_id));
		setVegasScriptHomeInCommandLine();
		CommandOutput commandOutput = ScriptRunner.run(getCommandLine(), 1, TimeUnit.MINUTES, isSilent());
		if (commandOutput.getResult() == 0) {
			Weblocation[] weblocationArray = (Weblocation[]) JsonManager.deserializeJson(commandOutput.getFile(),
					Weblocation[].class);
			this.weblocations = new ListOfWeblocations(new ArrayList<Weblocation>(Arrays.asList(weblocationArray)));
		} else {
			throw new Exception("ERROR getting weblocations from the " + this.sc_entity);
		}
	}

	public String getSc_entity() {
		return sc_entity;
	}

	public void setSc_entity(String sc_entity) {
		this.sc_entity = sc_entity;
	}

	public String getSc_interface_id() {
		return sc_interface_id;
	}

	public void setSc_interface_id(String sc_interface_id) {
		this.sc_interface_id = sc_interface_id;
	}

	public String getSc_web_loc_id() {
		return sc_web_loc_id;
	}

	public void setSc_web_loc_id(String sc_web_loc_id) {
		this.sc_web_loc_id = sc_web_loc_id;
	}
}
