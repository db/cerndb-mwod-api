package ch.cern.mwod.weblocations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.cern.mwod.command.CommandManager;
import ch.cern.mwod.commandline.CommandOutput;

@Service
public class DeleteWeblocationService {

	private CommandManager commandManager;
	private DeleteWeblocationCommand command;
	@Value("${WEBLOCATIONS_BASE_DN:xxx}")
	private String weblocationsBaseDN;

	public DeleteWeblocationService(CommandManager commandManager, DeleteWeblocationCommand command) {
		super();
		this.commandManager = commandManager;
		this.command = command;
	}

	public String delete(String sc_entity, String sc_interface_id, String sc_web_loc_id, boolean silent)
			throws Exception {
		String result = null;
		if (sc_entity != null && sc_interface_id != null) {
			command.setSilent(silent);
			command.setSc_entity(sc_entity);
			command.setSc_interface_id(sc_interface_id);
			command.setSc_web_loc_id(sc_web_loc_id);
			CommandOutput commandOutput = this.commandManager.execute(command);
			if (commandOutput != null) {
				if (commandOutput.getResult() == 0) {
					result = "SC-WEB-LOC-ID=" + sc_web_loc_id + ","
							+ weblocationsBaseDN
							.replace("$sc_entity", sc_entity)
							.replace("$sc_interface_id", sc_interface_id);		
				}
			}
		}
		return result;
	}
}
