package ch.cern.mwod.weblocations;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.cern.mwod.command.AbstractCommand;
import ch.cern.mwod.command.Command;
import ch.cern.mwod.commandline.ScriptRunner;

@Service
public class GetWeblocationsCommand extends AbstractCommand implements Command {

	private String sc_entity;
	private String sc_interface_id;
	@Value("${get_weblocations_command:xxx}")
	private String get_weblocations_command;
	
	@Override
	public Object execute() throws Exception {
		setCommandLine(get_weblocations_command);
		setCommandLine(getCommandLine().replace("$sc_entity", sc_entity));
		setCommandLine(getCommandLine().replace("$sc_interface_id", sc_interface_id));
		setVegasScriptHomeInCommandLine();
		return ScriptRunner.run(getCommandLine(), 1, TimeUnit.MINUTES, isSilent());
	}

	public String getSc_entity() {
		return sc_entity;
	}

	public void setSc_entity(String sc_entity) {
		this.sc_entity = sc_entity;
	}

	public String getSc_interface_id() {
		return sc_interface_id;
	}

	public void setSc_interface_id(String sc_interface_id) {
		this.sc_interface_id = sc_interface_id;
	}
	
	

}
