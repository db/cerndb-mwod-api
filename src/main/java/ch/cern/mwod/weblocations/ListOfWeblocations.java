package ch.cern.mwod.weblocations;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ListOfWeblocations {

	@JsonProperty("weblocations")
	private List<Weblocation> weblocations = new ArrayList<Weblocation>();

	public ListOfWeblocations(List<Weblocation> weblocations) {
		super();
		this.weblocations = weblocations;
	}

	@JsonProperty("weblocations")
	public List<Weblocation> getWeblocations() {
		return weblocations;
	}

	@JsonProperty("weblocations")
	public void setWeblocations(List<Weblocation> weblocations) {
		this.weblocations = weblocations;
	}

	public void addWeblocation(Weblocation weblocation) throws Exception {
		if (weblocation == null) {
			throw new Exception("ERROR: weblocation is null");
		}
		this.weblocations.add(weblocation);
	}

	public void removeWeblocation(Weblocation weblocation) throws Exception {
		if (weblocation == null) {
			throw new Exception("ERROR: weblocation is null");
		}
		if (!weblocations.contains(weblocation)) {
			throw new Exception("ERROR: weblocations does not contain " + weblocation);
		}
		this.weblocations.remove(weblocation);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(weblocations).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof ListOfWeblocations) == false) {
			return false;
		}
		ListOfWeblocations rhs = (ListOfWeblocations) other;
		return new EqualsBuilder().append(weblocations, rhs.weblocations).isEquals();
	}

	@Override
	public String toString() {
		return "ListOfWeblocations [weblocations=" + weblocations + "]";
	}
}
