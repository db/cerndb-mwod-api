package ch.cern.mwod.weblocations;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.cern.mwod.command.CommandManager;
import ch.cern.mwod.commandline.CommandOutput;
import ch.cern.mwod.commons.CoreCommons;
import ch.cern.mwod.json.JsonManager;

@Service
public class AddWeblocationService {

	private CommandManager commandManager;
	private AddWeblocationCommand command;
	@Value("${WEBLOCATIONS_BASE_DN:xxx}")
	private String weblocationsBaseDN;

	public AddWeblocationService(CommandManager commandManager, AddWeblocationCommand command) {
		super();
		this.commandManager = commandManager;
		this.command = command;
	}

	public String add(String sc_entity, String sc_interface_id, Weblocation weblocation, boolean silent)
			throws Exception {
		String result = null;
		if (weblocation != null) {
			Path dir = Paths.get(CoreCommons.TEMP_DIRECTORY);
			String json_file = Files
					.createTempFile(dir, Weblocation.class.getCanonicalName(), CoreCommons.JSON_FILE_SUFFIX).toString();
			JsonManager.serializeJson(weblocation, json_file);
			command.setSilent(silent);
			command.setJson_file(json_file);
			command.setSc_entity(sc_entity);
			command.setSc_interface_id(sc_interface_id);
			CommandOutput commandOutput = this.commandManager.execute(command);
			if (commandOutput != null) {
				if (commandOutput.getResult() == 0) {
					result = "SC-WEB-LOC-ID=" + weblocation.getScWebLocId() + "," + 
							weblocationsBaseDN
							.replace("$sc_entity", sc_entity)
							.replace("$sc_interface_id", sc_interface_id);
				}
			}
		}
		return result;
	}
}
