package ch.cern.mwod.weblocations;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeblocationController {

	@Autowired
	private GetWeblocationsService getWeblocationsService;
	@Autowired
	private AddWeblocationService addWeblocationService;
	@Autowired
	private DeleteWeblocationService deleteWeblocationService;

	@GetMapping("/entities/{sc_entity}/interfaces/{sc_interface_id}/weblocations")
	public ResponseEntity<Object> getWeblocations(@PathVariable String sc_entity, @PathVariable String sc_interface_id,
			@RequestParam(required = false, defaultValue = "true") boolean silent) {
		Weblocation[] weblocations = null;
		String decoded_sc_interface_id = new String(Base64.getDecoder().decode(sc_interface_id.getBytes()));
		ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(sc_entity + " " + decoded_sc_interface_id + "/weblocations");
		try {
			weblocations = this.getWeblocationsService.get(sc_entity, decoded_sc_interface_id, silent);
			if (weblocations != null) {
				responseEntity = ResponseEntity.status(HttpStatus.OK).body(weblocations);
			}
		} catch (Exception e) {
			responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
		return responseEntity;
	}

	@PostMapping("/entities/{sc_entity}/interfaces/{sc_interface_id}/weblocations")
	public ResponseEntity<Object> addWeblocation(@PathVariable String sc_entity, @PathVariable String sc_interface_id,
			@RequestBody Weblocation weblocation,
			@RequestParam(required = false, defaultValue = "true") boolean silent) {
		ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(weblocation);
		String decoded_sc_interface_id = new String(Base64.getDecoder().decode(sc_interface_id.getBytes()));
		try {
			String weblocationDN = this.addWeblocationService.add(sc_entity, decoded_sc_interface_id, weblocation,
					silent);
			if (weblocationDN != null) {
				responseEntity = ResponseEntity.status(HttpStatus.CREATED).body(weblocationDN);
			}
		} catch (Exception e) {
			responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
			e.printStackTrace();
		}
		return responseEntity;
	}

	@DeleteMapping("/entities/{sc_entity}/interfaces/{sc_interface_id}/weblocations/{sc_web_loc_id}")
	public ResponseEntity<Object> deleteWeblocation(@PathVariable String sc_entity,
			@PathVariable String sc_interface_id, @PathVariable String sc_web_loc_id,
			@RequestParam(required = false, defaultValue = "true") boolean silent) {
		ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND).body(sc_entity);
		String decoded_sc_interface_id = new String(Base64.getDecoder().decode(sc_interface_id.getBytes()));
		String decoded_sc_web_loc_id = new String(Base64.getDecoder().decode(sc_web_loc_id.getBytes()));
		try {
			String weblocationDN = this.deleteWeblocationService.delete(sc_entity, decoded_sc_interface_id,
					decoded_sc_web_loc_id, silent);
			if (weblocationDN != null) {
				responseEntity = ResponseEntity.status(HttpStatus.OK).body(weblocationDN);
			}
		} catch (Exception e) {
			responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
			e.printStackTrace();
		}
		return responseEntity;
	}
}
