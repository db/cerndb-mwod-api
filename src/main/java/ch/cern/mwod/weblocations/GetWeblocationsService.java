package ch.cern.mwod.weblocations;

import org.springframework.stereotype.Service;

import ch.cern.mwod.command.CommandManager;
import ch.cern.mwod.commandline.CommandOutput;
import ch.cern.mwod.json.JsonManager;

@Service
public class GetWeblocationsService {

	private CommandManager commandManager;
	private GetWeblocationsCommand command;

	public GetWeblocationsService(CommandManager commandManager, GetWeblocationsCommand command) {
		super();
		this.commandManager = commandManager;
		this.command = command;
	}

	public Weblocation[] get(String sc_entity, String sc_interface_id, boolean silent) throws Exception {
		command.setSc_entity(sc_entity);
		command.setSc_interface_id(sc_interface_id);
		command.setSilent(silent);
		CommandOutput commandOutput = this.commandManager.execute(command);
		Weblocation[] result = null;
		if (commandOutput != null) {
			result = (Weblocation[]) JsonManager
					.deserializeJson(commandOutput.getBufferedOutput().toString(), Weblocation[].class);
		}
		return result;
	}
}
