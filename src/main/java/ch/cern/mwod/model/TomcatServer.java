
package ch.cern.mwod.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "sc_tomcat_java_home",
    "sc_tomcat_server_https_port",
    "sc_tomcat_server_http_port",
    "sc_tomcat_server_name",
    "sc_tomcat_server_shutdown_port",
    "objectClass",
    "sc_tomcat_internal_ip",
    "objectclass",
    "sc_tomcat_server_jdk_version",
    "sc_tomcat_version",
    "sc_tomcat_server_java_options",
    "sc_tomcat_server_ajps_port",
    "sc_tomcat_server_https_host",
    "sc_tomcat_server_logging_properties",
    "sc_tomcat_haproxy_https_port",
    "sc_tomcat_server_ajp_port",
    "sc_tomcat_server_http_redirect_port",
    "sc_tomcat_haproxy_ajp_port",
    "sc_tomcat_server_catalina_options",
    "sc_tomcat_server_ajp_host",
    "sc_tomcat_haproxy_http_port",
    "sc_tomcat_container_id",
    "sc_tomcat_server_ajp_protocol",
    "sc_tomcat_application_ids",
    "sc_tomcat_server_http_host",
    "sc_tomcat_server_shutdown_key",
    "sc_tomcat_server_ajp_redirect_port"
})
public class TomcatServer {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_java_home")
    private String scTomcatJavaHome;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_server_https_port")
    private String scTomcatServerHttpsPort;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_server_http_port")
    private String scTomcatServerHttpPort;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_server_name")
    private String scTomcatServerName;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_server_shutdown_port")
    private String scTomcatServerShutdownPort;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("objectClass")
    private String objectClass;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_internal_ip")
    private String scTomcatInternalIp;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("objectclass")
    private String objectclass;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_server_jdk_version")
    private String scTomcatServerJdkVersion;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_version")
    private String scTomcatVersion;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_server_java_options")
    private String scTomcatServerJavaOptions;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_server_ajps_port")
    private String scTomcatServerAjpsPort;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_server_https_host")
    private String scTomcatServerHttpsHost;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_server_logging_properties")
    private String scTomcatServerLoggingProperties;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_haproxy_https_port")
    private String scTomcatHaproxyHttpsPort;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_server_ajp_port")
    private String scTomcatServerAjpPort;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_server_http_redirect_port")
    private String scTomcatServerHttpRedirectPort;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_haproxy_ajp_port")
    private String scTomcatHaproxyAjpPort;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_server_catalina_options")
    private String scTomcatServerCatalinaOptions;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_server_ajp_host")
    private String scTomcatServerAjpHost;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_haproxy_http_port")
    private String scTomcatHaproxyHttpPort;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_container_id")
    private String scTomcatContainerId;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_server_ajp_protocol")
    private String scTomcatServerAjpProtocol;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_application_ids")
    private String scTomcatApplicationIds;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_server_http_host")
    private String scTomcatServerHttpHost;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_server_shutdown_key")
    private String scTomcatServerShutdownKey;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_tomcat_server_ajp_redirect_port")
    private String scTomcatServerAjpRedirectPort;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatJavaHome
     */
    @JsonProperty("sc_tomcat_java_home")
    public String getScTomcatJavaHome() {
        return scTomcatJavaHome;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatJavaHome
     *     The sc_tomcat_java_home
     */
    @JsonProperty("sc_tomcat_java_home")
    public void setScTomcatJavaHome(String scTomcatJavaHome) {
        this.scTomcatJavaHome = scTomcatJavaHome;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatServerHttpsPort
     */
    @JsonProperty("sc_tomcat_server_https_port")
    public String getScTomcatServerHttpsPort() {
        return scTomcatServerHttpsPort;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatServerHttpsPort
     *     The sc_tomcat_server_https_port
     */
    @JsonProperty("sc_tomcat_server_https_port")
    public void setScTomcatServerHttpsPort(String scTomcatServerHttpsPort) {
        this.scTomcatServerHttpsPort = scTomcatServerHttpsPort;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatServerHttpPort
     */
    @JsonProperty("sc_tomcat_server_http_port")
    public String getScTomcatServerHttpPort() {
        return scTomcatServerHttpPort;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatServerHttpPort
     *     The sc_tomcat_server_http_port
     */
    @JsonProperty("sc_tomcat_server_http_port")
    public void setScTomcatServerHttpPort(String scTomcatServerHttpPort) {
        this.scTomcatServerHttpPort = scTomcatServerHttpPort;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatServerName
     */
    @JsonProperty("sc_tomcat_server_name")
    public String getScTomcatServerName() {
        return scTomcatServerName;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatServerName
     *     The sc_tomcat_server_name
     */
    @JsonProperty("sc_tomcat_server_name")
    public void setScTomcatServerName(String scTomcatServerName) {
        this.scTomcatServerName = scTomcatServerName;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatServerShutdownPort
     */
    @JsonProperty("sc_tomcat_server_shutdown_port")
    public String getScTomcatServerShutdownPort() {
        return scTomcatServerShutdownPort;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatServerShutdownPort
     *     The sc_tomcat_server_shutdown_port
     */
    @JsonProperty("sc_tomcat_server_shutdown_port")
    public void setScTomcatServerShutdownPort(String scTomcatServerShutdownPort) {
        this.scTomcatServerShutdownPort = scTomcatServerShutdownPort;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The objectClass
     */
    @JsonProperty("objectClass")
    public String getObjectClass() {
        return objectClass;
    }

    /**
     * 
     * (Required)
     * 
     * @param objectClass
     *     The objectClass
     */
    @JsonProperty("objectClass")
    public void setObjectClass(String objectClass) {
        this.objectClass = objectClass;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatInternalIp
     */
    @JsonProperty("sc_tomcat_internal_ip")
    public String getScTomcatInternalIp() {
        return scTomcatInternalIp;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatInternalIp
     *     The sc_tomcat_internal_ip
     */
    @JsonProperty("sc_tomcat_internal_ip")
    public void setScTomcatInternalIp(String scTomcatInternalIp) {
        this.scTomcatInternalIp = scTomcatInternalIp;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The objectclass
     */
    @JsonProperty("objectclass")
    public String getObjectclass() {
        return objectclass;
    }

    /**
     * 
     * (Required)
     * 
     * @param objectclass
     *     The objectclass
     */
    @JsonProperty("objectclass")
    public void setObjectclass(String objectclass) {
        this.objectclass = objectclass;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatServerJdkVersion
     */
    @JsonProperty("sc_tomcat_server_jdk_version")
    public String getScTomcatServerJdkVersion() {
        return scTomcatServerJdkVersion;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatServerJdkVersion
     *     The sc_tomcat_server_jdk_version
     */
    @JsonProperty("sc_tomcat_server_jdk_version")
    public void setScTomcatServerJdkVersion(String scTomcatServerJdkVersion) {
        this.scTomcatServerJdkVersion = scTomcatServerJdkVersion;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatVersion
     */
    @JsonProperty("sc_tomcat_version")
    public String getScTomcatVersion() {
        return scTomcatVersion;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatVersion
     *     The sc_tomcat_version
     */
    @JsonProperty("sc_tomcat_version")
    public void setScTomcatVersion(String scTomcatVersion) {
        this.scTomcatVersion = scTomcatVersion;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatServerJavaOptions
     */
    @JsonProperty("sc_tomcat_server_java_options")
    public String getScTomcatServerJavaOptions() {
        return scTomcatServerJavaOptions;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatServerJavaOptions
     *     The sc_tomcat_server_java_options
     */
    @JsonProperty("sc_tomcat_server_java_options")
    public void setScTomcatServerJavaOptions(String scTomcatServerJavaOptions) {
        this.scTomcatServerJavaOptions = scTomcatServerJavaOptions;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatServerAjpsPort
     */
    @JsonProperty("sc_tomcat_server_ajps_port")
    public String getScTomcatServerAjpsPort() {
        return scTomcatServerAjpsPort;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatServerAjpsPort
     *     The sc_tomcat_server_ajps_port
     */
    @JsonProperty("sc_tomcat_server_ajps_port")
    public void setScTomcatServerAjpsPort(String scTomcatServerAjpsPort) {
        this.scTomcatServerAjpsPort = scTomcatServerAjpsPort;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatServerHttpsHost
     */
    @JsonProperty("sc_tomcat_server_https_host")
    public String getScTomcatServerHttpsHost() {
        return scTomcatServerHttpsHost;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatServerHttpsHost
     *     The sc_tomcat_server_https_host
     */
    @JsonProperty("sc_tomcat_server_https_host")
    public void setScTomcatServerHttpsHost(String scTomcatServerHttpsHost) {
        this.scTomcatServerHttpsHost = scTomcatServerHttpsHost;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatServerLoggingProperties
     */
    @JsonProperty("sc_tomcat_server_logging_properties")
    public String getScTomcatServerLoggingProperties() {
        return scTomcatServerLoggingProperties;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatServerLoggingProperties
     *     The sc_tomcat_server_logging_properties
     */
    @JsonProperty("sc_tomcat_server_logging_properties")
    public void setScTomcatServerLoggingProperties(String scTomcatServerLoggingProperties) {
        this.scTomcatServerLoggingProperties = scTomcatServerLoggingProperties;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatHaproxyHttpsPort
     */
    @JsonProperty("sc_tomcat_haproxy_https_port")
    public String getScTomcatHaproxyHttpsPort() {
        return scTomcatHaproxyHttpsPort;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatHaproxyHttpsPort
     *     The sc_tomcat_haproxy_https_port
     */
    @JsonProperty("sc_tomcat_haproxy_https_port")
    public void setScTomcatHaproxyHttpsPort(String scTomcatHaproxyHttpsPort) {
        this.scTomcatHaproxyHttpsPort = scTomcatHaproxyHttpsPort;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatServerAjpPort
     */
    @JsonProperty("sc_tomcat_server_ajp_port")
    public String getScTomcatServerAjpPort() {
        return scTomcatServerAjpPort;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatServerAjpPort
     *     The sc_tomcat_server_ajp_port
     */
    @JsonProperty("sc_tomcat_server_ajp_port")
    public void setScTomcatServerAjpPort(String scTomcatServerAjpPort) {
        this.scTomcatServerAjpPort = scTomcatServerAjpPort;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatServerHttpRedirectPort
     */
    @JsonProperty("sc_tomcat_server_http_redirect_port")
    public String getScTomcatServerHttpRedirectPort() {
        return scTomcatServerHttpRedirectPort;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatServerHttpRedirectPort
     *     The sc_tomcat_server_http_redirect_port
     */
    @JsonProperty("sc_tomcat_server_http_redirect_port")
    public void setScTomcatServerHttpRedirectPort(String scTomcatServerHttpRedirectPort) {
        this.scTomcatServerHttpRedirectPort = scTomcatServerHttpRedirectPort;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatHaproxyAjpPort
     */
    @JsonProperty("sc_tomcat_haproxy_ajp_port")
    public String getScTomcatHaproxyAjpPort() {
        return scTomcatHaproxyAjpPort;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatHaproxyAjpPort
     *     The sc_tomcat_haproxy_ajp_port
     */
    @JsonProperty("sc_tomcat_haproxy_ajp_port")
    public void setScTomcatHaproxyAjpPort(String scTomcatHaproxyAjpPort) {
        this.scTomcatHaproxyAjpPort = scTomcatHaproxyAjpPort;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatServerCatalinaOptions
     */
    @JsonProperty("sc_tomcat_server_catalina_options")
    public String getScTomcatServerCatalinaOptions() {
        return scTomcatServerCatalinaOptions;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatServerCatalinaOptions
     *     The sc_tomcat_server_catalina_options
     */
    @JsonProperty("sc_tomcat_server_catalina_options")
    public void setScTomcatServerCatalinaOptions(String scTomcatServerCatalinaOptions) {
        this.scTomcatServerCatalinaOptions = scTomcatServerCatalinaOptions;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatServerAjpHost
     */
    @JsonProperty("sc_tomcat_server_ajp_host")
    public String getScTomcatServerAjpHost() {
        return scTomcatServerAjpHost;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatServerAjpHost
     *     The sc_tomcat_server_ajp_host
     */
    @JsonProperty("sc_tomcat_server_ajp_host")
    public void setScTomcatServerAjpHost(String scTomcatServerAjpHost) {
        this.scTomcatServerAjpHost = scTomcatServerAjpHost;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatHaproxyHttpPort
     */
    @JsonProperty("sc_tomcat_haproxy_http_port")
    public String getScTomcatHaproxyHttpPort() {
        return scTomcatHaproxyHttpPort;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatHaproxyHttpPort
     *     The sc_tomcat_haproxy_http_port
     */
    @JsonProperty("sc_tomcat_haproxy_http_port")
    public void setScTomcatHaproxyHttpPort(String scTomcatHaproxyHttpPort) {
        this.scTomcatHaproxyHttpPort = scTomcatHaproxyHttpPort;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatContainerId
     */
    @JsonProperty("sc_tomcat_container_id")
    public String getScTomcatContainerId() {
        return scTomcatContainerId;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatContainerId
     *     The sc_tomcat_container_id
     */
    @JsonProperty("sc_tomcat_container_id")
    public void setScTomcatContainerId(String scTomcatContainerId) {
        this.scTomcatContainerId = scTomcatContainerId;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatServerAjpProtocol
     */
    @JsonProperty("sc_tomcat_server_ajp_protocol")
    public String getScTomcatServerAjpProtocol() {
        return scTomcatServerAjpProtocol;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatServerAjpProtocol
     *     The sc_tomcat_server_ajp_protocol
     */
    @JsonProperty("sc_tomcat_server_ajp_protocol")
    public void setScTomcatServerAjpProtocol(String scTomcatServerAjpProtocol) {
        this.scTomcatServerAjpProtocol = scTomcatServerAjpProtocol;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatApplicationIds
     */
    @JsonProperty("sc_tomcat_application_ids")
    public String getScTomcatApplicationIds() {
        return scTomcatApplicationIds;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatApplicationIds
     *     The sc_tomcat_application_ids
     */
    @JsonProperty("sc_tomcat_application_ids")
    public void setScTomcatApplicationIds(String scTomcatApplicationIds) {
        this.scTomcatApplicationIds = scTomcatApplicationIds;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatServerHttpHost
     */
    @JsonProperty("sc_tomcat_server_http_host")
    public String getScTomcatServerHttpHost() {
        return scTomcatServerHttpHost;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatServerHttpHost
     *     The sc_tomcat_server_http_host
     */
    @JsonProperty("sc_tomcat_server_http_host")
    public void setScTomcatServerHttpHost(String scTomcatServerHttpHost) {
        this.scTomcatServerHttpHost = scTomcatServerHttpHost;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatServerShutdownKey
     */
    @JsonProperty("sc_tomcat_server_shutdown_key")
    public String getScTomcatServerShutdownKey() {
        return scTomcatServerShutdownKey;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatServerShutdownKey
     *     The sc_tomcat_server_shutdown_key
     */
    @JsonProperty("sc_tomcat_server_shutdown_key")
    public void setScTomcatServerShutdownKey(String scTomcatServerShutdownKey) {
        this.scTomcatServerShutdownKey = scTomcatServerShutdownKey;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTomcatServerAjpRedirectPort
     */
    @JsonProperty("sc_tomcat_server_ajp_redirect_port")
    public String getScTomcatServerAjpRedirectPort() {
        return scTomcatServerAjpRedirectPort;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTomcatServerAjpRedirectPort
     *     The sc_tomcat_server_ajp_redirect_port
     */
    @JsonProperty("sc_tomcat_server_ajp_redirect_port")
    public void setScTomcatServerAjpRedirectPort(String scTomcatServerAjpRedirectPort) {
        this.scTomcatServerAjpRedirectPort = scTomcatServerAjpRedirectPort;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(scTomcatJavaHome).append(scTomcatServerHttpsPort).append(scTomcatServerHttpPort).append(scTomcatServerName).append(scTomcatServerShutdownPort).append(objectClass).append(scTomcatInternalIp).append(objectclass).append(scTomcatServerJdkVersion).append(scTomcatVersion).append(scTomcatServerJavaOptions).append(scTomcatServerAjpsPort).append(scTomcatServerHttpsHost).append(scTomcatServerLoggingProperties).append(scTomcatHaproxyHttpsPort).append(scTomcatServerAjpPort).append(scTomcatServerHttpRedirectPort).append(scTomcatHaproxyAjpPort).append(scTomcatServerCatalinaOptions).append(scTomcatServerAjpHost).append(scTomcatHaproxyHttpPort).append(scTomcatContainerId).append(scTomcatServerAjpProtocol).append(scTomcatApplicationIds).append(scTomcatServerHttpHost).append(scTomcatServerShutdownKey).append(scTomcatServerAjpRedirectPort).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof TomcatServer) == false) {
            return false;
        }
        TomcatServer rhs = ((TomcatServer) other);
        return new EqualsBuilder().append(scTomcatJavaHome, rhs.scTomcatJavaHome).append(scTomcatServerHttpsPort, rhs.scTomcatServerHttpsPort).append(scTomcatServerHttpPort, rhs.scTomcatServerHttpPort).append(scTomcatServerName, rhs.scTomcatServerName).append(scTomcatServerShutdownPort, rhs.scTomcatServerShutdownPort).append(objectClass, rhs.objectClass).append(scTomcatInternalIp, rhs.scTomcatInternalIp).append(objectclass, rhs.objectclass).append(scTomcatServerJdkVersion, rhs.scTomcatServerJdkVersion).append(scTomcatVersion, rhs.scTomcatVersion).append(scTomcatServerJavaOptions, rhs.scTomcatServerJavaOptions).append(scTomcatServerAjpsPort, rhs.scTomcatServerAjpsPort).append(scTomcatServerHttpsHost, rhs.scTomcatServerHttpsHost).append(scTomcatServerLoggingProperties, rhs.scTomcatServerLoggingProperties).append(scTomcatHaproxyHttpsPort, rhs.scTomcatHaproxyHttpsPort).append(scTomcatServerAjpPort, rhs.scTomcatServerAjpPort).append(scTomcatServerHttpRedirectPort, rhs.scTomcatServerHttpRedirectPort).append(scTomcatHaproxyAjpPort, rhs.scTomcatHaproxyAjpPort).append(scTomcatServerCatalinaOptions, rhs.scTomcatServerCatalinaOptions).append(scTomcatServerAjpHost, rhs.scTomcatServerAjpHost).append(scTomcatHaproxyHttpPort, rhs.scTomcatHaproxyHttpPort).append(scTomcatContainerId, rhs.scTomcatContainerId).append(scTomcatServerAjpProtocol, rhs.scTomcatServerAjpProtocol).append(scTomcatApplicationIds, rhs.scTomcatApplicationIds).append(scTomcatServerHttpHost, rhs.scTomcatServerHttpHost).append(scTomcatServerShutdownKey, rhs.scTomcatServerShutdownKey).append(scTomcatServerAjpRedirectPort, rhs.scTomcatServerAjpRedirectPort).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
