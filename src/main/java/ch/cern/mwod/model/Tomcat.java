
package ch.cern.mwod.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "sc_domain", "sc_installation_directory", "sc_version", "sc_unix_owner",
		"sc_interface_commands_group", "nfs-volumes", "objectClass", "interfaces", "objectclass",
		"sc_installation_directory2", "sc_category", "sc_run_as", "sc_os", "sc_automatic_startup",
		"sc_configuration_directory", "sc_system_commands_group", "sc_comment", "hosts", "addresses", "sc_entity",
		"sc_logs_owner", "sc_sort_profile", "ping-entities", "sc_profile", "sc_project", "sc_type", "tomcat-servers",
		"sc_state", "sc_subcategory", "sc_profile_group", "sc_startup_order" })
public class Tomcat {

	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_domain")
	private String scDomain;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_installation_directory")
	private String scInstallationDirectory;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_version")
	private String scVersion;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_unix_owner")
	private String scUnixOwner;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_interface_commands_group")
	private String scInterfaceCommandsGroup;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("nfs-volumes")
	private List<Object> nfsVolumes = new ArrayList<Object>();
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("objectClass")
	private String objectClass;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("interfaces")
	private List<Object> interfaces = new ArrayList<Object>();
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("objectclass")
	private String objectclass;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_installation_directory2")
	private String scInstallationDirectory2;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_category")
	private String scCategory;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_run_as")
	private String scRunAs;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_os")
	private String scOs;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_automatic_startup")
	private String scAutomaticStartup;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_configuration_directory")
	private String scConfigurationDirectory;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_system_commands_group")
	private String scSystemCommandsGroup;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_comment")
	private String scComment;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("hosts")
	private List<Host> hosts = new ArrayList<Host>();
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("addresses")
	private List<Object> addresses = new ArrayList<Object>();
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_entity")
	private String scEntity;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_logs_owner")
	private String scLogsOwner;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_sort_profile")
	private String scSortProfile;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("ping-entities")
	private List<Object> pingEntities = new ArrayList<Object>();
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_profile")
	private String scProfile;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_project")
	private String scProject;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_type")
	private String scType;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("tomcat-servers")
	private List<TomcatServer> tomcatServers = new ArrayList<TomcatServer>();
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_state")
	private String scState;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_subcategory")
	private String scSubcategory;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_profile_group")
	private String scProfileGroup;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("sc_startup_order")
	private String scStartupOrder;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scDomain
	 */
	@JsonProperty("sc_domain")
	public String getScDomain() {
		return scDomain;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scDomain
	 *            The sc_domain
	 */
	@JsonProperty("sc_domain")
	public void setScDomain(String scDomain) {
		this.scDomain = scDomain;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scInstallationDirectory
	 */
	@JsonProperty("sc_installation_directory")
	public String getScInstallationDirectory() {
		return scInstallationDirectory;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scInstallationDirectory
	 *            The sc_installation_directory
	 */
	@JsonProperty("sc_installation_directory")
	public void setScInstallationDirectory(String scInstallationDirectory) {
		this.scInstallationDirectory = scInstallationDirectory;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scVersion
	 */
	@JsonProperty("sc_version")
	public String getScVersion() {
		return scVersion;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scVersion
	 *            The sc_version
	 */
	@JsonProperty("sc_version")
	public void setScVersion(String scVersion) {
		this.scVersion = scVersion;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scUnixOwner
	 */
	@JsonProperty("sc_unix_owner")
	public String getScUnixOwner() {
		return scUnixOwner;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scUnixOwner
	 *            The sc_unix_owner
	 */
	@JsonProperty("sc_unix_owner")
	public void setScUnixOwner(String scUnixOwner) {
		this.scUnixOwner = scUnixOwner;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scInterfaceCommandsGroup
	 */
	@JsonProperty("sc_interface_commands_group")
	public String getScInterfaceCommandsGroup() {
		return scInterfaceCommandsGroup;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scInterfaceCommandsGroup
	 *            The sc_interface_commands_group
	 */
	@JsonProperty("sc_interface_commands_group")
	public void setScInterfaceCommandsGroup(String scInterfaceCommandsGroup) {
		this.scInterfaceCommandsGroup = scInterfaceCommandsGroup;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The nfsVolumes
	 */
	@JsonProperty("nfs-volumes")
	public List<Object> getNfsVolumes() {
		return nfsVolumes;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param nfsVolumes
	 *            The nfs-volumes
	 */
	@JsonProperty("nfs-volumes")
	public void setNfsVolumes(List<Object> nfsVolumes) {
		this.nfsVolumes = nfsVolumes;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The objectClass
	 */
	@JsonProperty("objectClass")
	public String getObjectClass() {
		return objectClass;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param objectClass
	 *            The objectClass
	 */
	@JsonProperty("objectClass")
	public void setObjectClass(String objectClass) {
		this.objectClass = objectClass;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The interfaces
	 */
	@JsonProperty("interfaces")
	public List<Object> getInterfaces() {
		return interfaces;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param interfaces
	 *            The interfaces
	 */
	@JsonProperty("interfaces")
	public void setInterfaces(List<Object> interfaces) {
		this.interfaces = interfaces;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The objectclass
	 */
	@JsonProperty("objectclass")
	public String getObjectclass() {
		return objectclass;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param objectclass
	 *            The objectclass
	 */
	@JsonProperty("objectclass")
	public void setObjectclass(String objectclass) {
		this.objectclass = objectclass;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scInstallationDirectory2
	 */
	@JsonProperty("sc_installation_directory2")
	public String getScInstallationDirectory2() {
		return scInstallationDirectory2;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scInstallationDirectory2
	 *            The sc_installation_directory2
	 */
	@JsonProperty("sc_installation_directory2")
	public void setScInstallationDirectory2(String scInstallationDirectory2) {
		this.scInstallationDirectory2 = scInstallationDirectory2;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scCategory
	 */
	@JsonProperty("sc_category")
	public String getScCategory() {
		return scCategory;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scCategory
	 *            The sc_category
	 */
	@JsonProperty("sc_category")
	public void setScCategory(String scCategory) {
		this.scCategory = scCategory;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scRunAs
	 */
	@JsonProperty("sc_run_as")
	public String getScRunAs() {
		return scRunAs;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scRunAs
	 *            The sc_run_as
	 */
	@JsonProperty("sc_run_as")
	public void setScRunAs(String scRunAs) {
		this.scRunAs = scRunAs;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scOs
	 */
	@JsonProperty("sc_os")
	public String getScOs() {
		return scOs;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scOs
	 *            The sc_os
	 */
	@JsonProperty("sc_os")
	public void setScOs(String scOs) {
		this.scOs = scOs;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scAutomaticStartup
	 */
	@JsonProperty("sc_automatic_startup")
	public String getScAutomaticStartup() {
		return scAutomaticStartup;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scAutomaticStartup
	 *            The sc_automatic_startup
	 */
	@JsonProperty("sc_automatic_startup")
	public void setScAutomaticStartup(String scAutomaticStartup) {
		this.scAutomaticStartup = scAutomaticStartup;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scConfigurationDirectory
	 */
	@JsonProperty("sc_configuration_directory")
	public String getScConfigurationDirectory() {
		return scConfigurationDirectory;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scConfigurationDirectory
	 *            The sc_configuration_directory
	 */
	@JsonProperty("sc_configuration_directory")
	public void setScConfigurationDirectory(String scConfigurationDirectory) {
		this.scConfigurationDirectory = scConfigurationDirectory;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scSystemCommandsGroup
	 */
	@JsonProperty("sc_system_commands_group")
	public String getScSystemCommandsGroup() {
		return scSystemCommandsGroup;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scSystemCommandsGroup
	 *            The sc_system_commands_group
	 */
	@JsonProperty("sc_system_commands_group")
	public void setScSystemCommandsGroup(String scSystemCommandsGroup) {
		this.scSystemCommandsGroup = scSystemCommandsGroup;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scComment
	 */
	@JsonProperty("sc_comment")
	public String getScComment() {
		return scComment;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scComment
	 *            The sc_comment
	 */
	@JsonProperty("sc_comment")
	public void setScComment(String scComment) {
		this.scComment = scComment;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The hosts
	 */
	@JsonProperty("hosts")
	public List<Host> getHosts() {
		return hosts;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param hosts
	 *            The hosts
	 */
	@JsonProperty("hosts")
	public void setHosts(List<Host> hosts) {
		this.hosts = hosts;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The addresses
	 */
	@JsonProperty("addresses")
	public List<Object> getAddresses() {
		return addresses;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param addresses
	 *            The addresses
	 */
	@JsonProperty("addresses")
	public void setAddresses(List<Object> addresses) {
		this.addresses = addresses;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scEntity
	 */
	@JsonProperty("sc_entity")
	public String getScEntity() {
		return scEntity;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scEntity
	 *            The sc_entity
	 */
	@JsonProperty("sc_entity")
	public void setScEntity(String scEntity) {
		this.scEntity = scEntity;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scLogsOwner
	 */
	@JsonProperty("sc_logs_owner")
	public String getScLogsOwner() {
		return scLogsOwner;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scLogsOwner
	 *            The sc_logs_owner
	 */
	@JsonProperty("sc_logs_owner")
	public void setScLogsOwner(String scLogsOwner) {
		this.scLogsOwner = scLogsOwner;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scSortProfile
	 */
	@JsonProperty("sc_sort_profile")
	public String getScSortProfile() {
		return scSortProfile;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scSortProfile
	 *            The sc_sort_profile
	 */
	@JsonProperty("sc_sort_profile")
	public void setScSortProfile(String scSortProfile) {
		this.scSortProfile = scSortProfile;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The pingEntities
	 */
	@JsonProperty("ping-entities")
	public List<Object> getPingEntities() {
		return pingEntities;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param pingEntities
	 *            The ping-entities
	 */
	@JsonProperty("ping-entities")
	public void setPingEntities(List<Object> pingEntities) {
		this.pingEntities = pingEntities;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scProfile
	 */
	@JsonProperty("sc_profile")
	public String getScProfile() {
		return scProfile;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scProfile
	 *            The sc_profile
	 */
	@JsonProperty("sc_profile")
	public void setScProfile(String scProfile) {
		this.scProfile = scProfile;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scProject
	 */
	@JsonProperty("sc_project")
	public String getScProject() {
		return scProject;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scProject
	 *            The sc_project
	 */
	@JsonProperty("sc_project")
	public void setScProject(String scProject) {
		this.scProject = scProject;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scType
	 */
	@JsonProperty("sc_type")
	public String getScType() {
		return scType;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scType
	 *            The sc_type
	 */
	@JsonProperty("sc_type")
	public void setScType(String scType) {
		this.scType = scType;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The tomcatServers
	 */
	@JsonProperty("tomcat-servers")
	public List<TomcatServer> getTomcatServers() {
		return tomcatServers;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param tomcatServers
	 *            The tomcat-servers
	 */
	@JsonProperty("tomcat-servers")
	public void setTomcatServers(List<TomcatServer> tomcatServers) {
		this.tomcatServers = tomcatServers;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scState
	 */
	@JsonProperty("sc_state")
	public String getScState() {
		return scState;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scState
	 *            The sc_state
	 */
	@JsonProperty("sc_state")
	public void setScState(String scState) {
		this.scState = scState;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scSubcategory
	 */
	@JsonProperty("sc_subcategory")
	public String getScSubcategory() {
		return scSubcategory;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scSubcategory
	 *            The sc_subcategory
	 */
	@JsonProperty("sc_subcategory")
	public void setScSubcategory(String scSubcategory) {
		this.scSubcategory = scSubcategory;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scProfileGroup
	 */
	@JsonProperty("sc_profile_group")
	public String getScProfileGroup() {
		return scProfileGroup;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scProfileGroup
	 *            The sc_profile_group
	 */
	@JsonProperty("sc_profile_group")
	public void setScProfileGroup(String scProfileGroup) {
		this.scProfileGroup = scProfileGroup;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @return The scStartupOrder
	 */
	@JsonProperty("sc_startup_order")
	public String getScStartupOrder() {
		return scStartupOrder;
	}

	/**
	 * 
	 * (Required)
	 * 
	 * @param scStartupOrder
	 *            The sc_startup_order
	 */
	@JsonProperty("sc_startup_order")
	public void setScStartupOrder(String scStartupOrder) {
		this.scStartupOrder = scStartupOrder;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(scDomain).append(scInstallationDirectory).append(scVersion)
				.append(scUnixOwner).append(scInterfaceCommandsGroup).append(nfsVolumes).append(objectClass)
				.append(interfaces).append(objectclass).append(scInstallationDirectory2).append(scCategory)
				.append(scRunAs).append(scOs).append(scAutomaticStartup).append(scConfigurationDirectory)
				.append(scSystemCommandsGroup).append(scComment).append(hosts).append(addresses).append(scEntity)
				.append(scLogsOwner).append(scSortProfile).append(pingEntities).append(scProfile).append(scProject)
				.append(scType).append(tomcatServers).append(scState).append(scSubcategory).append(scProfileGroup)
				.append(scStartupOrder).append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Tomcat) == false) {
			return false;
		}
		Tomcat rhs = ((Tomcat) other);
		return new EqualsBuilder().append(scDomain, rhs.scDomain)
				.append(scInstallationDirectory, rhs.scInstallationDirectory).append(scVersion, rhs.scVersion)
				.append(scUnixOwner, rhs.scUnixOwner).append(scInterfaceCommandsGroup, rhs.scInterfaceCommandsGroup)
				.append(nfsVolumes, rhs.nfsVolumes).append(objectClass, rhs.objectClass)
				.append(interfaces, rhs.interfaces).append(objectclass, rhs.objectclass)
				.append(scInstallationDirectory2, rhs.scInstallationDirectory2).append(scCategory, rhs.scCategory)
				.append(scRunAs, rhs.scRunAs).append(scOs, rhs.scOs).append(scAutomaticStartup, rhs.scAutomaticStartup)
				.append(scConfigurationDirectory, rhs.scConfigurationDirectory)
				.append(scSystemCommandsGroup, rhs.scSystemCommandsGroup).append(scComment, rhs.scComment)
				.append(hosts, rhs.hosts).append(addresses, rhs.addresses).append(scEntity, rhs.scEntity)
				.append(scLogsOwner, rhs.scLogsOwner).append(scSortProfile, rhs.scSortProfile)
				.append(pingEntities, rhs.pingEntities).append(scProfile, rhs.scProfile)
				.append(scProject, rhs.scProject).append(scType, rhs.scType).append(tomcatServers, rhs.tomcatServers)
				.append(scState, rhs.scState).append(scSubcategory, rhs.scSubcategory)
				.append(scProfileGroup, rhs.scProfileGroup).append(scStartupOrder, rhs.scStartupOrder)
				.append(additionalProperties, rhs.additionalProperties).isEquals();
	}

}
