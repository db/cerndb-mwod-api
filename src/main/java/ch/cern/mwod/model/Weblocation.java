
package ch.cern.mwod.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "objectClass",
    "sc_web_loc_jk_socket_keepalive",
    "sc_web_loc_server_entity",
    "sc_web_loc_jk_server_name",
    "sc_web_loc_dest_path",
    "sc_web_loc_type",
    "objectclass",
    "sc_web_loc_jk_type",
    "sc_web_loc_order",
    "sc_web_loc_jk_protocol",
    "sc_web_loc_auth_type",
    "sc_web_loc_id"
})
public class Weblocation {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("objectClass")
    private String objectClass;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_web_loc_jk_socket_keepalive")
    private String scWebLocJkSocketKeepalive;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_web_loc_server_entity")
    private String scWebLocServerEntity;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_web_loc_jk_server_name")
    private String scWebLocJkServerName;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_web_loc_dest_path")
    private String scWebLocDestPath;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_web_loc_type")
    private String scWebLocType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("objectclass")
    private String objectclass;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_web_loc_jk_type")
    private String scWebLocJkType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_web_loc_order")
    private String scWebLocOrder;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_web_loc_jk_protocol")
    private String scWebLocJkProtocol;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_web_loc_auth_type")
    private String scWebLocAuthType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_web_loc_id")
    private String scWebLocId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     * @return
     *     The objectClass
     */
    @JsonProperty("objectClass")
    public String getObjectClass() {
        return objectClass;
    }

    /**
     * 
     * (Required)
     * 
     * @param objectClass
     *     The objectClass
     */
    @JsonProperty("objectClass")
    public void setObjectClass(String objectClass) {
        this.objectClass = objectClass;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scWebLocJkSocketKeepalive
     */
    @JsonProperty("sc_web_loc_jk_socket_keepalive")
    public String getScWebLocJkSocketKeepalive() {
        return scWebLocJkSocketKeepalive;
    }

    /**
     * 
     * (Required)
     * 
     * @param scWebLocJkSocketKeepalive
     *     The sc_web_loc_jk_socket_keepalive
     */
    @JsonProperty("sc_web_loc_jk_socket_keepalive")
    public void setScWebLocJkSocketKeepalive(String scWebLocJkSocketKeepalive) {
        this.scWebLocJkSocketKeepalive = scWebLocJkSocketKeepalive;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scWebLocServerEntity
     */
    @JsonProperty("sc_web_loc_server_entity")
    public String getScWebLocServerEntity() {
        return scWebLocServerEntity;
    }

    /**
     * 
     * (Required)
     * 
     * @param scWebLocServerEntity
     *     The sc_web_loc_server_entity
     */
    @JsonProperty("sc_web_loc_server_entity")
    public void setScWebLocServerEntity(String scWebLocServerEntity) {
        this.scWebLocServerEntity = scWebLocServerEntity;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scWebLocJkServerName
     */
    @JsonProperty("sc_web_loc_jk_server_name")
    public String getScWebLocJkServerName() {
        return scWebLocJkServerName;
    }

    /**
     * 
     * (Required)
     * 
     * @param scWebLocJkServerName
     *     The sc_web_loc_jk_server_name
     */
    @JsonProperty("sc_web_loc_jk_server_name")
    public void setScWebLocJkServerName(String scWebLocJkServerName) {
        this.scWebLocJkServerName = scWebLocJkServerName;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scWebLocDestPath
     */
    @JsonProperty("sc_web_loc_dest_path")
    public String getScWebLocDestPath() {
        return scWebLocDestPath;
    }

    /**
     * 
     * (Required)
     * 
     * @param scWebLocDestPath
     *     The sc_web_loc_dest_path
     */
    @JsonProperty("sc_web_loc_dest_path")
    public void setScWebLocDestPath(String scWebLocDestPath) {
        this.scWebLocDestPath = scWebLocDestPath;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scWebLocType
     */
    @JsonProperty("sc_web_loc_type")
    public String getScWebLocType() {
        return scWebLocType;
    }

    /**
     * 
     * (Required)
     * 
     * @param scWebLocType
     *     The sc_web_loc_type
     */
    @JsonProperty("sc_web_loc_type")
    public void setScWebLocType(String scWebLocType) {
        this.scWebLocType = scWebLocType;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The objectclass
     */
    @JsonProperty("objectclass")
    public String getObjectclass() {
        return objectclass;
    }

    /**
     * 
     * (Required)
     * 
     * @param objectclass
     *     The objectclass
     */
    @JsonProperty("objectclass")
    public void setObjectclass(String objectclass) {
        this.objectclass = objectclass;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scWebLocJkType
     */
    @JsonProperty("sc_web_loc_jk_type")
    public String getScWebLocJkType() {
        return scWebLocJkType;
    }

    /**
     * 
     * (Required)
     * 
     * @param scWebLocJkType
     *     The sc_web_loc_jk_type
     */
    @JsonProperty("sc_web_loc_jk_type")
    public void setScWebLocJkType(String scWebLocJkType) {
        this.scWebLocJkType = scWebLocJkType;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scWebLocOrder
     */
    @JsonProperty("sc_web_loc_order")
    public String getScWebLocOrder() {
        return scWebLocOrder;
    }

    /**
     * 
     * (Required)
     * 
     * @param scWebLocOrder
     *     The sc_web_loc_order
     */
    @JsonProperty("sc_web_loc_order")
    public void setScWebLocOrder(String scWebLocOrder) {
        this.scWebLocOrder = scWebLocOrder;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scWebLocJkProtocol
     */
    @JsonProperty("sc_web_loc_jk_protocol")
    public String getScWebLocJkProtocol() {
        return scWebLocJkProtocol;
    }

    /**
     * 
     * (Required)
     * 
     * @param scWebLocJkProtocol
     *     The sc_web_loc_jk_protocol
     */
    @JsonProperty("sc_web_loc_jk_protocol")
    public void setScWebLocJkProtocol(String scWebLocJkProtocol) {
        this.scWebLocJkProtocol = scWebLocJkProtocol;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scWebLocAuthType
     */
    @JsonProperty("sc_web_loc_auth_type")
    public String getScWebLocAuthType() {
        return scWebLocAuthType;
    }

    /**
     * 
     * (Required)
     * 
     * @param scWebLocAuthType
     *     The sc_web_loc_auth_type
     */
    @JsonProperty("sc_web_loc_auth_type")
    public void setScWebLocAuthType(String scWebLocAuthType) {
        this.scWebLocAuthType = scWebLocAuthType;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scWebLocId
     */
    @JsonProperty("sc_web_loc_id")
    public String getScWebLocId() {
        return scWebLocId;
    }

    /**
     * 
     * (Required)
     * 
     * @param scWebLocId
     *     The sc_web_loc_id
     */
    @JsonProperty("sc_web_loc_id")
    public void setScWebLocId(String scWebLocId) {
        this.scWebLocId = scWebLocId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(objectClass).append(scWebLocJkSocketKeepalive).append(scWebLocServerEntity).append(scWebLocJkServerName).append(scWebLocDestPath).append(scWebLocType).append(objectclass).append(scWebLocJkType).append(scWebLocOrder).append(scWebLocJkProtocol).append(scWebLocAuthType).append(scWebLocId).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Weblocation) == false) {
            return false;
        }
        Weblocation rhs = ((Weblocation) other);
        return new EqualsBuilder().append(objectClass, rhs.objectClass).append(scWebLocJkSocketKeepalive, rhs.scWebLocJkSocketKeepalive).append(scWebLocServerEntity, rhs.scWebLocServerEntity).append(scWebLocJkServerName, rhs.scWebLocJkServerName).append(scWebLocDestPath, rhs.scWebLocDestPath).append(scWebLocType, rhs.scWebLocType).append(objectclass, rhs.objectclass).append(scWebLocJkType, rhs.scWebLocJkType).append(scWebLocOrder, rhs.scWebLocOrder).append(scWebLocJkProtocol, rhs.scWebLocJkProtocol).append(scWebLocAuthType, rhs.scWebLocAuthType).append(scWebLocId, rhs.scWebLocId).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
