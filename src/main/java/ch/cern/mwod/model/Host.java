
package ch.cern.mwod.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "objectClass",
    "objectclass",
    "sc_host_id",
    "sc_host_name",
    "sc_login_as"
})
public class Host {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("objectClass")
    private String objectClass;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("objectclass")
    private String objectclass;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_host_id")
    private String scHostId;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_host_name")
    private String scHostName;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_login_as")
    private String scLoginAs;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     * @return
     *     The objectClass
     */
    @JsonProperty("objectClass")
    public String getObjectClass() {
        return objectClass;
    }

    /**
     * 
     * (Required)
     * 
     * @param objectClass
     *     The objectClass
     */
    @JsonProperty("objectClass")
    public void setObjectClass(String objectClass) {
        this.objectClass = objectClass;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The objectclass
     */
    @JsonProperty("objectclass")
    public String getObjectclass() {
        return objectclass;
    }

    /**
     * 
     * (Required)
     * 
     * @param objectclass
     *     The objectclass
     */
    @JsonProperty("objectclass")
    public void setObjectclass(String objectclass) {
        this.objectclass = objectclass;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scHostId
     */
    @JsonProperty("sc_host_id")
    public String getScHostId() {
        return scHostId;
    }

    /**
     * 
     * (Required)
     * 
     * @param scHostId
     *     The sc_host_id
     */
    @JsonProperty("sc_host_id")
    public void setScHostId(String scHostId) {
        this.scHostId = scHostId;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scHostName
     */
    @JsonProperty("sc_host_name")
    public String getScHostName() {
        return scHostName;
    }

    /**
     * 
     * (Required)
     * 
     * @param scHostName
     *     The sc_host_name
     */
    @JsonProperty("sc_host_name")
    public void setScHostName(String scHostName) {
        this.scHostName = scHostName;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scLoginAs
     */
    @JsonProperty("sc_login_as")
    public String getScLoginAs() {
        return scLoginAs;
    }

    /**
     * 
     * (Required)
     * 
     * @param scLoginAs
     *     The sc_login_as
     */
    @JsonProperty("sc_login_as")
    public void setScLoginAs(String scLoginAs) {
        this.scLoginAs = scLoginAs;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(objectClass).append(objectclass).append(scHostId).append(scHostName).append(scLoginAs).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Host) == false) {
            return false;
        }
        Host rhs = ((Host) other);
        return new EqualsBuilder().append(objectClass, rhs.objectClass).append(objectclass, rhs.objectclass).append(scHostId, rhs.scHostId).append(scHostName, rhs.scHostName).append(scLoginAs, rhs.scLoginAs).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
