
package ch.cern.mwod.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "sc_category",
    "objectClass"
})
public class ScCategory {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_category")
    private String scCategory;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("objectClass")
    private String objectClass;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scCategory
     */
    @JsonProperty("sc_category")
    public String getScCategory() {
        return scCategory;
    }

    /**
     * 
     * (Required)
     * 
     * @param scCategory
     *     The sc_category
     */
    @JsonProperty("sc_category")
    public void setScCategory(String scCategory) {
        this.scCategory = scCategory;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The objectClass
     */
    @JsonProperty("objectClass")
    public String getObjectClass() {
        return objectClass;
    }

    /**
     * 
     * (Required)
     * 
     * @param objectClass
     *     The objectClass
     */
    @JsonProperty("objectClass")
    public void setObjectClass(String objectClass) {
        this.objectClass = objectClass;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(scCategory).append(objectClass).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ScCategory) == false) {
            return false;
        }
        ScCategory rhs = ((ScCategory) other);
        return new EqualsBuilder().append(scCategory, rhs.scCategory).append(objectClass, rhs.objectClass).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
