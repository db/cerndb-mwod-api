package ch.cern.mwod.commons;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class LoggingInterceptor implements HandlerInterceptor {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object arg2, Exception arg3)
			throws Exception {
		logResponse(response);
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object arg2,
			ModelAndView modelAndView) throws Exception {
		if (modelAndView != null) {
			Map<String, Object> items = modelAndView.getModel();
			items.forEach((k, v) -> logger.debug(k + " " + v.getClass().getName()));
		} else {
			logger.debug("postHandle: NO ModelAndView");
		}
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object arg2) throws Exception {
		logRequest(request);
		return true;
	}

	private void logRequest(HttpServletRequest request) {
		Enumeration<String> headers = request.getHeaderNames();
		while (headers.hasMoreElements()) {
			String headerName = (String) headers.nextElement();
			logger.debug(headerName + ": " + request.getHeader(headerName));
		}
	}

	private void logResponse(HttpServletResponse response) {
		Collection<String> headers = response.getHeaderNames();
		for (String headerName : headers) {
			logger.debug(headerName + ": " + response.getHeader(headerName));
		}
	}

}
