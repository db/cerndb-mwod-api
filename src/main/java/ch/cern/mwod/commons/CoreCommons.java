package ch.cern.mwod.commons;

import org.springframework.core.io.ClassPathResource;

public class CoreCommons {

	public final static String TEMP_DIRECTORY = System.getProperty("java.io.tmpdir");
	public final static String FILE_SEPARATOR = System.getProperty("file.separator");
	public final static String JSON_FILE_SUFFIX = ".json";
	public static final String DELETED = "DELETED";

	public static ClassPathResource getClassPathResource(String path, Class<?> cl) {
		return new ClassPathResource(path, cl);
	}
}
