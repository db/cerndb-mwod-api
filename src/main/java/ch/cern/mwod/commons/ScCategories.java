package ch.cern.mwod.commons;

public enum ScCategories {
	WEBLOCATIONS("weblocations"), INTERFACES("interfaces");

	private final String name;

	private ScCategories(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
