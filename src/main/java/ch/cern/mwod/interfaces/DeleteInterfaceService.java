package ch.cern.mwod.interfaces;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.cern.mwod.command.CommandManager;
import ch.cern.mwod.commandline.CommandOutput;

@Service
public class DeleteInterfaceService {

	private CommandManager commandManager;
	private DeleteInterfaceCommand command;
	@Value("${INTERFACES_BASE_DN:xxx}")
	private String interfacesBaseDN;

	public DeleteInterfaceService(CommandManager commandManager, DeleteInterfaceCommand command) {
		super();
		this.commandManager = commandManager;
		this.command = command;
	}

	public String delete(String sc_entity, String sc_interface_id, boolean silent) throws Exception {
		String result = null;
		if (sc_entity != null && sc_interface_id != null) {
			command.setSilent(silent);
			command.setSc_entity(sc_entity);
			command.setSc_interface_id(sc_interface_id);
			CommandOutput commandOutput = this.commandManager.execute(command);
			if (commandOutput != null) {
				if (commandOutput.getResult() == 0) {
					result = "SC-INTERFACE-ID=" + sc_interface_id + "," + interfacesBaseDN.replace("$sc_entity", sc_entity);
				}
			}
		}
		return result;
	}
}
