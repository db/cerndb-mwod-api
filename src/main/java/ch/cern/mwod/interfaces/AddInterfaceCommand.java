package ch.cern.mwod.interfaces;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.cern.mwod.command.AbstractCommand;
import ch.cern.mwod.command.Command;
import ch.cern.mwod.commandline.ScriptRunner;

@Service
public class AddInterfaceCommand extends AbstractCommand implements Command {

	private String sc_entity;
	private String json_file;
	@Value("${INTERFACES_BASE_DN:xxx}")
	private String interfacesBaseDN;
	@Value("${add_interface_command:xxx}")
	private String add_interface_command;

	@Override
	public Object execute() throws Exception {
		this.interfacesBaseDN = this.interfacesBaseDN.replace("$sc_entity", this.sc_entity);
		setCommandLine(add_interface_command);
		setVegasScriptHomeInCommandLine();
		setCommandLine(getCommandLine().replace("$base_dn", this.interfacesBaseDN));
		setCommandLine(getCommandLine().replace("$json_file", json_file));
		setLdapPortHostInCommandLine();
		return ScriptRunner.run(getCommandLine(), 1, TimeUnit.MINUTES, isSilent());
	}

	public String getSc_entity() {
		return sc_entity;
	}

	public void setSc_entity(String sc_entity) {
		this.sc_entity = sc_entity;
	}

	public String getJson_file() {
		return json_file;
	}

	public void setJson_file(String json_file) {
		this.json_file = json_file;
	}
}
