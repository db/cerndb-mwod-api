package ch.cern.mwod.interfaces;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InterfaceController {

	@Autowired
	private GetInterfaceService getInterfaceService;
	@Autowired
	private AddInterfaceService addInterfaceService;
	@Autowired
	private DeleteInterfaceService deleteInterfaceService;

	@GetMapping("/entities/{sc_entity}/interfaces/{sc_interface_id}")
	public ResponseEntity<Object> getInterface(@PathVariable String sc_entity, @PathVariable String sc_interface_id,
			@RequestParam(required = false, defaultValue = "true") boolean silent) {
		Interface intfc = null;
		String decoded_sc_interface_id = new String(Base64.getDecoder().decode(sc_interface_id.getBytes()));
		ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(sc_entity + " " + decoded_sc_interface_id);
		try {
			intfc = this.getInterfaceService.get(sc_entity, decoded_sc_interface_id, silent);
			if (intfc != null) {
				responseEntity = ResponseEntity.status(HttpStatus.OK).body(intfc);
			}
		} catch (Exception e) {
			responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
		return responseEntity;
	}

	@PostMapping("/entities/{sc_entity}/interfaces")
	public ResponseEntity<Object> addInterface(@PathVariable String sc_entity, @RequestBody Interface intfc,
			@RequestParam(required = false, defaultValue = "true") boolean silent) {
		ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(intfc);
		try {
			String interfaceDN = this.addInterfaceService.add(sc_entity, intfc, silent);
			if (interfaceDN != null) {
				responseEntity = ResponseEntity.status(HttpStatus.CREATED).body(interfaceDN);
			}
		} catch (Exception e) {
			responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
			e.printStackTrace();
		}

		return responseEntity;
	}

	@DeleteMapping("/entities/{sc_entity}/interfaces/{sc_interface_id}")
	public ResponseEntity<Object> deleteInterface(@PathVariable String sc_entity, @PathVariable String sc_interface_id,
			@RequestParam(required = false, defaultValue = "true") boolean silent) {
		ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND).body(sc_entity);
		String decoded_sc_interface_id = new String(Base64.getDecoder().decode(sc_interface_id.getBytes()));
		try {
			String interfaceDN = this.deleteInterfaceService.delete(sc_entity, decoded_sc_interface_id, silent);
			if (interfaceDN != null) {
				responseEntity = ResponseEntity.status(HttpStatus.OK).body(interfaceDN);
			}
		} catch (Exception e) {
			responseEntity = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
			e.printStackTrace();
		}
		return responseEntity;
	}
}
