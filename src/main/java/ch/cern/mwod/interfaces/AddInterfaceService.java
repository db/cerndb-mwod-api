package ch.cern.mwod.interfaces;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import ch.cern.mwod.command.CommandManager;
import ch.cern.mwod.commandline.CommandOutput;
import ch.cern.mwod.commons.CoreCommons;
import ch.cern.mwod.json.JsonManager;

@Service
public class AddInterfaceService {

	private CommandManager commandManager;
	private AddInterfaceCommand command;
	@Value("${INTERFACES_BASE_DN:xxx}")
	private String interfacesBaseDN;

	public AddInterfaceService(CommandManager commandManager, AddInterfaceCommand command) {
		super();
		this.commandManager = commandManager;
		this.command = command;
	}

	public String add(String sc_entity, Interface intfc, boolean silent) throws Exception {
		String result = null;
		if (intfc != null) {
			Path dir = Paths.get(CoreCommons.TEMP_DIRECTORY);
			String json_file = Files.createTempFile(dir, Interface.class.getCanonicalName(), ".json").toString();
			JsonManager.serializeJson(intfc, json_file);
			command.setSilent(silent);
			command.setJson_file(json_file);
			command.setSc_entity(sc_entity);
			CommandOutput commandOutput = this.commandManager.execute(command);
			if (commandOutput != null) {
				if (commandOutput.getResult() == 0) {
					result = "SC-INTERFACE-ID=" + intfc.getScInterfaceId() + ","
							+ interfacesBaseDN.replace("$sc_entity", sc_entity);
				}
			}
		}
		return result;
	}
}
