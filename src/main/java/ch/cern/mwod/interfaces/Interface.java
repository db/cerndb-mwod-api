
package ch.cern.mwod.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import ch.cern.mwod.weblocations.Weblocation;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "sc_interface_ssl_certificate_key_file",
    "sc_interface_port",
    "objectClass",
    "sc_interface_type",
    "sc_default_interface",
    "sc_template_name",
    "sc_interface_ssl_certificate_chain_file",
    "weblocations",
    "sc_interface_global_id",
    "sc_address_ref",
    "sc_interface_ssl_certificate_file",
    "objectclass",
    "sc_interface_domain",
    "sc_included",
    "sc_interface_id",
    "sc_important",
    "sc_interface_protocol"
})
public class Interface {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_interface_ssl_certificate_key_file")
    private String scInterfaceSslCertificateKeyFile;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_interface_port")
    private String scInterfacePort;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("objectClass")
    private String objectClass;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_interface_type")
    private String scInterfaceType;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_default_interface")
    private String scDefaultInterface;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_template_name")
    private String scTemplateName;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_interface_ssl_certificate_chain_file")
    private String scInterfaceSslCertificateChainFile;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("weblocations")
    private List<Weblocation> weblocations = new ArrayList<Weblocation>();
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_interface_global_id")
    private String scInterfaceGlobalId;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_address_ref")
    private List<String> scAddressRef = new ArrayList<String>();
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_interface_ssl_certificate_file")
    private String scInterfaceSslCertificateFile;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("objectclass")
    private String objectclass;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_interface_domain")
    private String scInterfaceDomain;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_included")
    private String scIncluded;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_interface_id")
    private String scInterfaceId;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_important")
    private String scImportant;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("sc_interface_protocol")
    private String scInterfaceProtocol;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scInterfaceSslCertificateKeyFile
     */
    @JsonProperty("sc_interface_ssl_certificate_key_file")
    public String getScInterfaceSslCertificateKeyFile() {
        return scInterfaceSslCertificateKeyFile;
    }

    /**
     * 
     * (Required)
     * 
     * @param scInterfaceSslCertificateKeyFile
     *     The sc_interface_ssl_certificate_key_file
     */
    @JsonProperty("sc_interface_ssl_certificate_key_file")
    public void setScInterfaceSslCertificateKeyFile(String scInterfaceSslCertificateKeyFile) {
        this.scInterfaceSslCertificateKeyFile = scInterfaceSslCertificateKeyFile;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scInterfacePort
     */
    @JsonProperty("sc_interface_port")
    public String getScInterfacePort() {
        return scInterfacePort;
    }

    /**
     * 
     * (Required)
     * 
     * @param scInterfacePort
     *     The sc_interface_port
     */
    @JsonProperty("sc_interface_port")
    public void setScInterfacePort(String scInterfacePort) {
        this.scInterfacePort = scInterfacePort;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The objectClass
     */
    @JsonProperty("objectClass")
    public String getObjectClass() {
        return objectClass;
    }

    /**
     * 
     * (Required)
     * 
     * @param objectClass
     *     The objectClass
     */
    @JsonProperty("objectClass")
    public void setObjectClass(String objectClass) {
        this.objectClass = objectClass;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scInterfaceType
     */
    @JsonProperty("sc_interface_type")
    public String getScInterfaceType() {
        return scInterfaceType;
    }

    /**
     * 
     * (Required)
     * 
     * @param scInterfaceType
     *     The sc_interface_type
     */
    @JsonProperty("sc_interface_type")
    public void setScInterfaceType(String scInterfaceType) {
        this.scInterfaceType = scInterfaceType;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scDefaultInterface
     */
    @JsonProperty("sc_default_interface")
    public String getScDefaultInterface() {
        return scDefaultInterface;
    }

    /**
     * 
     * (Required)
     * 
     * @param scDefaultInterface
     *     The sc_default_interface
     */
    @JsonProperty("sc_default_interface")
    public void setScDefaultInterface(String scDefaultInterface) {
        this.scDefaultInterface = scDefaultInterface;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scTemplateName
     */
    @JsonProperty("sc_template_name")
    public String getScTemplateName() {
        return scTemplateName;
    }

    /**
     * 
     * (Required)
     * 
     * @param scTemplateName
     *     The sc_template_name
     */
    @JsonProperty("sc_template_name")
    public void setScTemplateName(String scTemplateName) {
        this.scTemplateName = scTemplateName;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scInterfaceSslCertificateChainFile
     */
    @JsonProperty("sc_interface_ssl_certificate_chain_file")
    public String getScInterfaceSslCertificateChainFile() {
        return scInterfaceSslCertificateChainFile;
    }

    /**
     * 
     * (Required)
     * 
     * @param scInterfaceSslCertificateChainFile
     *     The sc_interface_ssl_certificate_chain_file
     */
    @JsonProperty("sc_interface_ssl_certificate_chain_file")
    public void setScInterfaceSslCertificateChainFile(String scInterfaceSslCertificateChainFile) {
        this.scInterfaceSslCertificateChainFile = scInterfaceSslCertificateChainFile;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The weblocations
     */
    @JsonProperty("weblocations")
    public List<Weblocation> getWeblocations() {
        return weblocations;
    }

    /**
     * 
     * (Required)
     * 
     * @param weblocations
     *     The weblocations
     */
    @JsonProperty("weblocations")
    public void setWeblocations(List<Weblocation> weblocations) {
        this.weblocations = weblocations;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scInterfaceGlobalId
     */
    @JsonProperty("sc_interface_global_id")
    public String getScInterfaceGlobalId() {
        return scInterfaceGlobalId;
    }

    /**
     * 
     * (Required)
     * 
     * @param scInterfaceGlobalId
     *     The sc_interface_global_id
     */
    @JsonProperty("sc_interface_global_id")
    public void setScInterfaceGlobalId(String scInterfaceGlobalId) {
        this.scInterfaceGlobalId = scInterfaceGlobalId;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scAddressRef
     */
    @JsonProperty("sc_address_ref")
    public List<String> getScAddressRef() {
        return scAddressRef;
    }

    /**
     * 
     * (Required)
     * 
     * @param scAddressRef
     *     The sc_address_ref
     */
    @JsonProperty("sc_address_ref")
    public void setScAddressRef(List<String> scAddressRef) {
        this.scAddressRef = scAddressRef;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scInterfaceSslCertificateFile
     */
    @JsonProperty("sc_interface_ssl_certificate_file")
    public String getScInterfaceSslCertificateFile() {
        return scInterfaceSslCertificateFile;
    }

    /**
     * 
     * (Required)
     * 
     * @param scInterfaceSslCertificateFile
     *     The sc_interface_ssl_certificate_file
     */
    @JsonProperty("sc_interface_ssl_certificate_file")
    public void setScInterfaceSslCertificateFile(String scInterfaceSslCertificateFile) {
        this.scInterfaceSslCertificateFile = scInterfaceSslCertificateFile;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The objectclass
     */
    @JsonProperty("objectclass")
    public String getObjectclass() {
        return objectclass;
    }

    /**
     * 
     * (Required)
     * 
     * @param objectclass
     *     The objectclass
     */
    @JsonProperty("objectclass")
    public void setObjectclass(String objectclass) {
        this.objectclass = objectclass;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scInterfaceDomain
     */
    @JsonProperty("sc_interface_domain")
    public String getScInterfaceDomain() {
        return scInterfaceDomain;
    }

    /**
     * 
     * (Required)
     * 
     * @param scInterfaceDomain
     *     The sc_interface_domain
     */
    @JsonProperty("sc_interface_domain")
    public void setScInterfaceDomain(String scInterfaceDomain) {
        this.scInterfaceDomain = scInterfaceDomain;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scIncluded
     */
    @JsonProperty("sc_included")
    public String getScIncluded() {
        return scIncluded;
    }

    /**
     * 
     * (Required)
     * 
     * @param scIncluded
     *     The sc_included
     */
    @JsonProperty("sc_included")
    public void setScIncluded(String scIncluded) {
        this.scIncluded = scIncluded;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scInterfaceId
     */
    @JsonProperty("sc_interface_id")
    public String getScInterfaceId() {
        return scInterfaceId;
    }

    /**
     * 
     * (Required)
     * 
     * @param scInterfaceId
     *     The sc_interface_id
     */
    @JsonProperty("sc_interface_id")
    public void setScInterfaceId(String scInterfaceId) {
        this.scInterfaceId = scInterfaceId;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scImportant
     */
    @JsonProperty("sc_important")
    public String getScImportant() {
        return scImportant;
    }

    /**
     * 
     * (Required)
     * 
     * @param scImportant
     *     The sc_important
     */
    @JsonProperty("sc_important")
    public void setScImportant(String scImportant) {
        this.scImportant = scImportant;
    }

    /**
     * 
     * (Required)
     * 
     * @return
     *     The scInterfaceProtocol
     */
    @JsonProperty("sc_interface_protocol")
    public String getScInterfaceProtocol() {
        return scInterfaceProtocol;
    }

    /**
     * 
     * (Required)
     * 
     * @param scInterfaceProtocol
     *     The sc_interface_protocol
     */
    @JsonProperty("sc_interface_protocol")
    public void setScInterfaceProtocol(String scInterfaceProtocol) {
        this.scInterfaceProtocol = scInterfaceProtocol;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(scInterfaceSslCertificateKeyFile).append(scInterfacePort).append(objectClass).append(scInterfaceType).append(scDefaultInterface).append(scTemplateName).append(scInterfaceSslCertificateChainFile).append(weblocations).append(scInterfaceGlobalId).append(scAddressRef).append(scInterfaceSslCertificateFile).append(objectclass).append(scInterfaceDomain).append(scIncluded).append(scInterfaceId).append(scImportant).append(scInterfaceProtocol).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Interface) == false) {
            return false;
        }
        Interface rhs = ((Interface) other);
        return new EqualsBuilder().append(scInterfaceSslCertificateKeyFile, rhs.scInterfaceSslCertificateKeyFile).append(scInterfacePort, rhs.scInterfacePort).append(objectClass, rhs.objectClass).append(scInterfaceType, rhs.scInterfaceType).append(scDefaultInterface, rhs.scDefaultInterface).append(scTemplateName, rhs.scTemplateName).append(scInterfaceSslCertificateChainFile, rhs.scInterfaceSslCertificateChainFile).append(weblocations, rhs.weblocations).append(scInterfaceGlobalId, rhs.scInterfaceGlobalId).append(scAddressRef, rhs.scAddressRef).append(scInterfaceSslCertificateFile, rhs.scInterfaceSslCertificateFile).append(objectclass, rhs.objectclass).append(scInterfaceDomain, rhs.scInterfaceDomain).append(scIncluded, rhs.scIncluded).append(scInterfaceId, rhs.scInterfaceId).append(scImportant, rhs.scImportant).append(scInterfaceProtocol, rhs.scInterfaceProtocol).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
