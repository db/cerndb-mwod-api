package ch.cern.mwod.interfaces;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import ch.cern.mwod.command.AbstractCommand;
import ch.cern.mwod.command.Command;
import ch.cern.mwod.commandline.CommandOutput;
import ch.cern.mwod.commandline.ScriptRunner;
import ch.cern.mwod.commons.CoreCommons;
import ch.cern.mwod.json.JsonManager;

@Service
public class DeleteInterfaceCommand extends AbstractCommand implements Command {

	private String sc_entity;
	private String sc_interface_id;
	private ListOfInterfaces interfaces;
	private Interface interfaceToDelete;
	private String baseDN;
	@Value("${add_interface_command:xxx}")
	private String add_interface_command;
	@Value("${get_interface_command:xxx}")
	private String get_interface_command;
	@Value("${get_interfaces_command:xxx}")
	private String get_interfaces_command;
	
	@Override
	public Object execute() throws Exception {
		getInterfaceToDelete();
		getAllInterfaces();
		removeInterfaceFromList();
		return deleteInterface();
	}

	private CommandOutput deleteInterface()
			throws JsonGenerationException, JsonMappingException, IOException, Exception {
		Path dir = Paths.get(CoreCommons.TEMP_DIRECTORY);
		String jsonFilePathname = Files.createTempFile(dir, "interfaces", ".json").toString();
		JsonManager.serializeJson(this.interfaces, jsonFilePathname);
		this.baseDN = "SC-ENTITY=" + this.sc_entity + ",SC-CATEGORY=entities,ou=syscontrol,dc=cern,dc=ch";
		setCommandLine(add_interface_command);
		setCommandLine(getCommandLine().replace("$base_dn", this.baseDN));
		setCommandLine(getCommandLine().replace("$json_file", jsonFilePathname));
		setVegasScriptHomeInCommandLine();
		setLdapPortHostInCommandLine();
		CommandOutput commandOutput = ScriptRunner.run(getCommandLine(), 1, TimeUnit.MINUTES, isSilent());
		return commandOutput;
	}

	private void removeInterfaceFromList() throws Exception {
		this.interfaces.removeInterface(this.interfaceToDelete);
	}

	private void getInterfaceToDelete() throws Exception {
		setCommandLine(get_interface_command);
		setCommandLine(getCommandLine().replace("$sc_entity", this.sc_entity));
		setCommandLine(getCommandLine().replace("$sc_interface_id", this.sc_interface_id));
		setVegasScriptHomeInCommandLine();
		CommandOutput commandOutput = ScriptRunner.run(getCommandLine(), 1, TimeUnit.MINUTES, this.isSilent());
		if (commandOutput.getResult() == 0) {
			interfaceToDelete = ((Interface[]) JsonManager.deserializeJson(commandOutput.getBufferedOutput().toString(),
					Interface[].class))[0];
		} else {
			throw new Exception("ERROR getting the interface " + sc_interface_id + " to delete from " + this.sc_entity);
		}
	}

	private void getAllInterfaces() throws Exception {
		setCommandLine(get_interfaces_command);
		setCommandLine(getCommandLine().replace("$sc_entity", this.sc_entity));
		setVegasScriptHomeInCommandLine();
		CommandOutput commandOutput = ScriptRunner.run(getCommandLine(), 1, TimeUnit.MINUTES, isSilent());
		if (commandOutput.getResult() == 0) {
			Interface[] interfaceArray = (Interface[]) JsonManager.deserializeJson(commandOutput.getFile(),
					Interface[].class);
			this.interfaces = new ListOfInterfaces(new ArrayList<Interface>(Arrays.asList(interfaceArray)));
		} else {
			throw new Exception("ERROR getting interfaces from the " + this.sc_entity);
		}
	}

	public String getSc_entity() {
		return sc_entity;
	}

	public void setSc_entity(String sc_entity) {
		this.sc_entity = sc_entity;
	}

	public String getSc_interface_id() {
		return sc_interface_id;
	}

	public void setSc_interface_id(String sc_interface_id) {
		this.sc_interface_id = sc_interface_id;
	}

	public String getBaseDN() {
		return baseDN;
	}

	public void setBaseDN(String baseDN) {
		this.baseDN = baseDN;
	}
}
