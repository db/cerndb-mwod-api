package ch.cern.mwod.interfaces;

import org.springframework.stereotype.Service;

import ch.cern.mwod.command.CommandManager;
import ch.cern.mwod.commandline.CommandOutput;
import ch.cern.mwod.json.JsonManager;

@Service
public class GetInterfaceService {

	private CommandManager commandManager;
	private GetInterfaceCommand command;

	public GetInterfaceService(CommandManager commandManager, GetInterfaceCommand command) {
		super();
		this.commandManager = commandManager;
		this.command = command;
	}

	public Interface get(String sc_entity, String sc_interface_id, boolean silent) throws Exception {
		command.setSc_entity(sc_entity);
		command.setSc_interface_id(sc_interface_id);
		command.setSilent(silent);
		CommandOutput commandOutput = this.commandManager.execute(command);
		Interface result = null;
		if (commandOutput != null) {
			Interface[] interfaces = (Interface[]) JsonManager.deserializeJson(commandOutput.getBufferedOutput().toString(),
					Interface[].class);
			if (interfaces != null && interfaces.length > 0) {
				result = interfaces[0];
			}
		}
		return result;
	}
}
