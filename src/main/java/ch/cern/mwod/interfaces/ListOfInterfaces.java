package ch.cern.mwod.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ListOfInterfaces {

	@JsonProperty("interfaces")
	private List<Interface> interfaces = new ArrayList<Interface>();

	public ListOfInterfaces(List<Interface> interfaces) {
		super();
		this.interfaces = interfaces;
	}

	@JsonProperty("interfaces")
	public List<Interface> getInterfaces() {
		return interfaces;
	}

	@JsonProperty("interfaces")
	public void setInterfaces(List<Interface> interfaces) {
		this.interfaces = interfaces;
	}

	public void addInterface(Interface intfc) throws Exception {
		if (intfc == null) {
			throw new Exception("ERROR: Interface is null");
		}
		this.interfaces.add(intfc);
	}

	public void removeInterface(Interface intfc) throws Exception {
		if (intfc == null) {
			throw new Exception("ERROR: Interface is null");
		}
		if (!interfaces.contains(intfc)) {
			throw new Exception("ERROR: interfaces does not contain " + intfc);
		}
		this.interfaces.remove(intfc);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(interfaces).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof ListOfInterfaces) == false) {
			return false;
		}
		ListOfInterfaces rhs = (ListOfInterfaces) other;
		return new EqualsBuilder().append(interfaces, rhs.getInterfaces()).isEquals();
	}

	@Override
	public String toString() {
		return "ListOfInterfaces [interfaces=" + interfaces + "]";
	}
}
