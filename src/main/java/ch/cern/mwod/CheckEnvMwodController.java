package ch.cern.mwod;

import java.util.Properties;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CheckEnvMwodController {
	
	@GetMapping("environment")
	public ResponseEntity<Properties> getEnvProperties(){
		ResponseEntity<Properties> responseEntity = ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		Properties properties = null;
		try {
			properties = System.getProperties();
			responseEntity = ResponseEntity.status(HttpStatus.OK).body(properties);
		} catch (Exception e) {
			ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
		return responseEntity;
	}
}
