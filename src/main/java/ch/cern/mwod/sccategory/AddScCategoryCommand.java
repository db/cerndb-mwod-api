package ch.cern.mwod.sccategory;

import java.util.concurrent.TimeUnit;

import ch.cern.mwod.command.Command;
import ch.cern.mwod.commandline.ScriptRunner;

public class AddScCategoryCommand implements Command {
	
	private String commandLine;
	private boolean silent;

	public AddScCategoryCommand(String jsonFilePathname, boolean silent) {
		this.commandLine = "/home/lurodrig/development/workspaces/cerndb-mwod-java/mwod-script-runner/src/test/resources/vegas-cli-add.sh "
				+ jsonFilePathname;
		this.silent = silent;
	}

	@Override
	public Object execute() throws Exception {
		return ScriptRunner.run(this.commandLine, 1, TimeUnit.MINUTES, this.silent);
	}

}
