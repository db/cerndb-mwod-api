package ch.cern.mwod.tomcats;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import ch.cern.mwod.command.CommandManager;
import ch.cern.mwod.model.Tomcat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TomcatServicesTest {

	@Autowired
	private GetTomcatService getTomcatService;
	@Autowired
	private AddTomcatService addTomcatService;
	@Autowired
	private DeleteTomcatService deleteTomcatService;
	@Autowired
	private ListTomcatService listTomcatService;
	@MockBean
	private CommandManager commandManager;
	@MockBean
	private GetTomcatCommand getTomcatCommand;
	@MockBean
	private AddTomcatCommand addTomcatCommand;
	@MockBean
	private DeleteTomcatCommand deleteTomcatCommand;
	@MockBean
	private ListTomcatCommand listTomcatCommand;
	private static TomcatTestsCommons testCommons;

	@BeforeClass
	public static void init() throws Exception {
		testCommons = new TomcatTestsCommons(TomcatServicesTest.class);
	}

	@Test
	public void testGetTomcatService_when_sc_entity_is_null() throws Exception {
		given(commandManager.execute(getTomcatCommand)).willReturn(testCommons.getEMPTY_COMMAND_OUTPUT());
		Tomcat result = getTomcatService.get(testCommons.NULL_SC_ENTITY, true);
		assertThat(result).isEqualTo(null);
	}

	@Test
	public void testGetTomcatService_when_tomcat_is_not_found() throws Exception {
		given(commandManager.execute(getTomcatCommand)).willReturn(testCommons.getEMPTY_COMMAND_OUTPUT());
		Tomcat result = getTomcatService.get(testCommons.NON_EXISTING_SC_ENTITY, true);
		assertThat(result).isEqualTo(null);
	}

	@Test
	public void testGetTomcatService_when_sc_entity_exists() throws Exception {
		given(commandManager.execute(getTomcatCommand)).willReturn(testCommons.getCOMMAND_OUTPUT());
		Tomcat result = getTomcatService.get(testCommons.getExpected_sc_entity(), true);
		assertThat(result).isInstanceOf(Tomcat.class);
	}
	
	@Test
	public void testDeleteTomcatService_when_sc_entity_exists() throws Exception {
		given(commandManager.execute(deleteTomcatCommand)).willReturn(testCommons.getCOMMAND_OUTPUT());
		String result = deleteTomcatService.delete(testCommons.getExpected_sc_entity(), true);
		assertThat(result).containsPattern(testCommons.getExpectedTomcatDNPattern());
	}

	@Test
	public void testAddTomcatService_when_input_is_ok() throws IOException, Exception {
		given(commandManager.execute(addTomcatCommand)).willReturn(testCommons.getCOMMAND_OUTPUT());
		String result = addTomcatService.add(testCommons.getTomcat(), true);
		assertThat(result).containsPattern(testCommons.getExpectedTomcatDNPattern());
	}
	
	@Test
	public void testListTomcatService() throws Exception {
		given(commandManager.execute(listTomcatCommand)).willReturn(testCommons.getCOMMAND_OUTPUT());
		Tomcat[] result = listTomcatService.list(true);
		assertThat(result).isNotEmpty();
	}
}
