package ch.cern.mwod.tomcats;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import ch.cern.mwod.json.JsonManager;

@RunWith(SpringRunner.class)
@WebMvcTest(TomcatController.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TomcatControllerTest {

	@MockBean
	private GetTomcatService getTomcatService;
	@MockBean
	private AddTomcatService addTomcatService;
	@MockBean
	private DeleteTomcatService deleteTomcatService;
	@MockBean
	private ListTomcatService listTomcatService;

	@Autowired
	private MockMvc mockMvc;
	private static TomcatTestsCommons testCommons;

	@BeforeClass
	public static void init() throws Exception {
		testCommons = new TomcatTestsCommons(TomcatControllerTest.class);
	}

	@Test
	public void test_A_get_Tomcat_when_sc_entity_not_exists() throws Exception {
		given(getTomcatService.get(testCommons.NON_EXISTING_SC_ENTITY, true)).willReturn(null);
		this.mockMvc
				.perform(get("/entities/tomcats/" + testCommons.NON_EXISTING_SC_ENTITY).accept(MediaType.TEXT_PLAIN))
				.andExpect(status().isNotFound());
	}

	@Test
	public void test_B_get_Tomcat_when_sc_entity_exists() throws Exception {
		given(getTomcatService.get(testCommons.getExpected_sc_entity(), true)).willReturn(testCommons.getTomcat());
		this.mockMvc
				.perform(get("/entities/tomcats/" + testCommons.getExpected_sc_entity()).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().json(JsonManager.deserializeJsonAsString(testCommons.getTomcat())));
	}

	@Test
	public void test_C_delete_Tomcat_when_sc_entity_exists() throws Exception {
		given(deleteTomcatService.delete(testCommons.getExpected_sc_entity(), true)).willReturn(testCommons.getExpectedTomcatDN());
		this.mockMvc.perform(delete("/entities/tomcats/" + testCommons.getExpected_sc_entity()).accept(MediaType.TEXT_PLAIN))
				.andExpect(status().isOk()).andExpect(content().string(testCommons.getExpectedTomcatDN()));
	}

	@Test
	public void test_D_add_Tomcat_when_input_is_correct() throws Exception {
		given(addTomcatService.add(testCommons.getTomcat(), true)).willReturn(testCommons.getExpectedTomcatDN());
		this.mockMvc
				.perform(post("/entities/tomcats").contentType(MediaType.APPLICATION_JSON)
						.content(JsonManager.deserializeJsonAsString(testCommons.getTomcat())))
				.andExpect(status().isCreated())
				.andExpect(content().string(testCommons.getExpectedTomcatDN()));
	}

	@Test
	public void test_E_list_Tomcats() throws Exception {
		given(listTomcatService.list(true)).willReturn(testCommons.getTomcats());
		this.mockMvc.perform(get("/entities/tomcats").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
}
