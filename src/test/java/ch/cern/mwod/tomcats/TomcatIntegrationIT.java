package ch.cern.mwod.tomcats;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import ch.cern.mwod.model.Tomcat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TomcatIntegrationIT {

	@Autowired
	private TestRestTemplate testRestTemplate;
	private static TomcatTestsCommons testCommons;

	@BeforeClass
	public static void init() throws Exception {
		testCommons = new TomcatTestsCommons(TomcatServicesTest.class);
	}
	
	@Test
	public void test_A_addTomcat_when_input_is_ok() throws Exception {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.TEXT_PLAIN));
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Tomcat> requestEntity = new HttpEntity<Tomcat>(testCommons.getTomcat(), headers);
		ResponseEntity<String> result = this.testRestTemplate.postForEntity("/entities/tomcats", requestEntity,
				String.class);
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		assertThat(result.getBody()).isEqualTo(testCommons.getExpectedTomcatDN());
		assertThat(result.getBody()).contains(testCommons.getExpected_sc_entity());
	}
	
	@Test
	public void test_B_getTomcat_when_sc_entity_exists() throws Exception {
		ResponseEntity<Tomcat> result = this.testRestTemplate
				.getForEntity("/entities/tomcats/" + testCommons.getExpected_sc_entity(), Tomcat.class);
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(result.getBody().getScEntity()).isEqualTo(testCommons.getExpected_sc_entity());
	}

	@Test
	public void test_C_getTomcat_when_sc_entity_not_exists() throws Exception {
		HttpStatus httpStatus = this.testRestTemplate
				.getForEntity("/entities/tomcats/" + testCommons.NON_EXISTING_SC_ENTITY, String.class).getStatusCode();
		assertThat(httpStatus).isEqualTo(HttpStatus.NOT_FOUND);
	}

	@Test
	public void test_D_deleteTomcat_when_sc_entity_exists() throws Exception {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.TEXT_PLAIN));
		HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
		ResponseEntity<String> result = this.testRestTemplate.exchange(
				"/entities/tomcats/" + testCommons.getExpected_sc_entity(), HttpMethod.DELETE, requestEntity, String.class);
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(result.getBody()).isEqualTo(testCommons.getExpectedTomcatDN());
	}
}
