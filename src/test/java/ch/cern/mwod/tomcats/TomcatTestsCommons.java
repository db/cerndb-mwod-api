package ch.cern.mwod.tomcats;

import java.nio.file.Files;

import org.apache.commons.lang.RandomStringUtils;

import ch.cern.mwod.TestCommons;
import ch.cern.mwod.commandline.CommandOutput;
import ch.cern.mwod.commons.CoreCommons;
import ch.cern.mwod.json.JsonManager;
import ch.cern.mwod.model.Tomcat;

public class TomcatTestsCommons extends TestCommons {

	public final String TOMCAT_JSON_PATH = "output_for_tomcat_docker_guys.json";
	public final String NON_EXISTING_SC_ENTITY = "tomcat_docker_devils";
	public final String NULL_SC_ENTITY = null;
	public String tomcatsBaseDN;
	private Tomcat tomcat;
	private String expected_sc_entity;
	private String expectedTomcatDN;
	private String expectedTomcatDNPattern = "SC-ENTITY=.*,SC-CATEGORY=entities,ou=syscontrol,dc=cern,dc=ch";
	private Tomcat[] tomcats;

	public TomcatTestsCommons(Class<?> cl) throws Exception {
		super(cl);
		if(System.getenv("TOMCATS_BASE_DN")!=null && !System.getenv("TOMCATS_BASE_DN").isEmpty()){
			tomcatsBaseDN = "SC-ENTITY=$sc_entity,SC-CATEGORY=entities,ou=syscontrol,dc=cern,dc=ch";
		}
		COMMAND_OUTPUT = new CommandOutput();
		COMMAND_OUTPUT.setResult(0);
		String output = new String();
		output = new String(
				Files.readAllBytes(CoreCommons.getClassPathResource(TOMCAT_JSON_PATH, cl).getFile().toPath()));
		COMMAND_OUTPUT.setBufferedOutput(new StringBuffer(output));
		tomcats = (Tomcat[]) JsonManager.deserializeJson(
				CoreCommons.getClassPathResource(TOMCAT_JSON_PATH, TomcatControllerTest.class).getFile(),
				Tomcat[].class);
		tomcat = tomcats[0];
		expected_sc_entity = "tomcat_mwm_" + RandomStringUtils.random(10, true, true);
		tomcat.setScEntity(expected_sc_entity);
		expectedTomcatDN = tomcatsBaseDN.replace("$sc_entity", tomcat.getScEntity());
	}

	public String getTomcatsBaseDN() {
		return tomcatsBaseDN;
	}

	public void setTomcatsBaseDN(String tomcatsBaseDN) {
		this.tomcatsBaseDN = tomcatsBaseDN;
	}

	public Tomcat getTomcat() {
		return tomcat;
	}

	public void setTomcat(Tomcat tomcat) {
		this.tomcat = tomcat;
	}

	public String getExpected_sc_entity() {
		return expected_sc_entity;
	}

	public void setExpected_sc_entity(String expected_sc_entity) {
		this.expected_sc_entity = expected_sc_entity;
	}

	public String getExpectedTomcatDN() {
		return expectedTomcatDN;
	}

	public void setExpectedTomcatDN(String expectedTomcatDN) {
		this.expectedTomcatDN = expectedTomcatDN;
	}

	public Tomcat[] getTomcats() {
		return tomcats;
	}

	public void setTomcats(Tomcat[] tomcats) {
		this.tomcats = tomcats;
	}

	public String getExpectedTomcatDNPattern() {
		return expectedTomcatDNPattern;
	}

	public void setExpectedTomcatDNPattern(String expectedTomcatDNPattern) {
		this.expectedTomcatDNPattern = expectedTomcatDNPattern;
	}
}
