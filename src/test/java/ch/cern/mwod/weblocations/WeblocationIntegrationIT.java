package ch.cern.mwod.weblocations;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WeblocationIntegrationIT {

	@Autowired
	private TestRestTemplate testRestTemplate;
	private static WeblocationsTestCommons testCommons;

	@BeforeClass
	public static void init() throws Exception {
		testCommons = new WeblocationsTestCommons(WeblocationIntegrationIT.class);
	}

	@Test
	public void test_A_add_Weblocation_when_input_is_ok() throws Exception {
		testCommons.addInterface(testRestTemplate);
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.TEXT_PLAIN));
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Weblocation> requestEntity = new HttpEntity<Weblocation>(testCommons.getWeblocation(), headers);
		ResponseEntity<String> result = testRestTemplate
				.postForEntity(
						"/entities/" + testCommons.APACHE_SC_ENTITY + "/interfaces/"
								+ testCommons.getEncoded_sc_interface_id() + "/weblocations",
						requestEntity, String.class);
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		assertThat(result.getBody()).isEqualTo(testCommons.getExpectedWeblocationDN());
	}

	@Test
	public void test_B_get_Weblocations_when_sc_entity_and_sc_interface_id_exist() throws Exception {
		ResponseEntity<Weblocation[]> result = testRestTemplate.getForEntity(testCommons.getGet_weblocations_url(),
				Weblocation[].class);
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(result.getBody().length).isGreaterThanOrEqualTo(0);
	}

	@Test
	public void test_C_deleteWeblocation_when_sc_entity_interface_id_and_webl_loc_id_exist() throws Exception {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.TEXT_PLAIN));
		HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
		ResponseEntity<String> result = testRestTemplate.exchange(testCommons.getDelete_weblocation_url(), HttpMethod.DELETE, requestEntity, String.class);
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(result.getBody()).isEqualTo(testCommons.getExpectedWeblocationDN());
		testCommons.deleteInterface(testRestTemplate);
	}
}
