package ch.cern.mwod.weblocations;

import java.util.Base64;

import org.apache.commons.lang.RandomStringUtils;

import ch.cern.mwod.interfaces.InterfaceTestsCommons;

public class WeblocationsTestCommons extends InterfaceTestsCommons {

	private String get_weblocations_url;
	private String get_weblocation_url;
	private String delete_weblocation_url;
	private Weblocation weblocation;
	private String expectedWeblocationDN;
	private String expected_sc_web_loc_id;
	private String encoded_sc_web_loc_id;
	private String weblocationsBaseDN = System.getenv("WEBLOCATIONS_BASE_DN");

	public WeblocationsTestCommons(Class<?> cl) throws Exception {
		super(cl);
		if (System.getenv("WEBLOCATIONS_BASE_DN") != null && !System.getenv("WEBLOCATIONS_BASE_DN").isEmpty()) {
			interfacesBaseDN = "SC-CATEGORY=weblocations,SC-INTERFACE-ID=$sc_interface_id,SC-CATEGORY=interfaces,SC-ENTITY=$sc_entity,SC-CATEGORY=entities,ou=syscontrol,dc=cern,dc=ch";
		}
		if (super.getIntfc().getWeblocations() != null && super.getIntfc().getWeblocations().size() > 0) {
			weblocation = super.getIntfc().getWeblocations().get(0);
			expected_sc_web_loc_id = RandomStringUtils.random(10, true, true);
			weblocation.setScWebLocId(expected_sc_web_loc_id);
			expectedWeblocationDN = "SC-WEB-LOC-ID=" + expected_sc_web_loc_id + "," + weblocationsBaseDN
					.replace("$sc_entity", APACHE_SC_ENTITY).replace("$sc_interface_id", getExpected_sc_interface_id());
			encoded_sc_web_loc_id = new String(Base64.getEncoder().encode(expected_sc_web_loc_id.getBytes()));
		}
		get_weblocations_url = "/entities/" + APACHE_SC_ENTITY + "/interfaces/" + super.getEncoded_sc_interface_id()
				+ "/weblocations";
		delete_weblocation_url = "/entities/" + APACHE_SC_ENTITY + "/interfaces/" + super.getEncoded_sc_interface_id()
				+ "/weblocations/" + encoded_sc_web_loc_id;
	}

	public Weblocation getWeblocation() {
		return weblocation;
	}

	public void setWeblocation(Weblocation weblocation) {
		this.weblocation = weblocation;
	}

	public String getExpectedWeblocationDN() {
		return expectedWeblocationDN;
	}

	public void setExpectedWeblocationDN(String expectedWeblocationDN) {
		this.expectedWeblocationDN = expectedWeblocationDN;
	}

	public String getExpected_sc_web_loc_id() {
		return expected_sc_web_loc_id;
	}

	public void setExpected_sc_web_loc_id(String expected_sc_web_loc_id) {
		this.expected_sc_web_loc_id = expected_sc_web_loc_id;
	}

	public String getGet_weblocations_url() {
		return get_weblocations_url;
	}

	public void setGet_weblocations_url(String get_weblocations_url) {
		this.get_weblocations_url = get_weblocations_url;
	}

	public String getGet_weblocation_url() {
		return get_weblocation_url;
	}

	public void setGet_weblocation_url(String get_weblocation_url) {
		this.get_weblocation_url = get_weblocation_url;
	}

	public String getDelete_weblocation_url() {
		return delete_weblocation_url;
	}

	public void setDelete_weblocation_url(String delete_weblocation_url) {
		this.delete_weblocation_url = delete_weblocation_url;
	}
}
