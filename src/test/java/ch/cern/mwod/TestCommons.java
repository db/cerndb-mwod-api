package ch.cern.mwod;

import java.io.IOException;
import java.nio.file.Files;

import ch.cern.mwod.commandline.CommandOutput;
import ch.cern.mwod.commons.CoreCommons;

public class TestCommons {

	public final String APACHE_SC_ENTITY = "apache_mwmgr_a_01_prod";
	public final String NOT_FOUND_JSON_PATH = "output_for_not_found.json";
	protected CommandOutput COMMAND_OUTPUT;
	protected CommandOutput EMPTY_COMMAND_OUTPUT;
	
	public TestCommons(Class<?> cl) throws IOException {
		EMPTY_COMMAND_OUTPUT = new CommandOutput();
		EMPTY_COMMAND_OUTPUT.setResult(0);
		String empty_output = new String(Files.readAllBytes(
				CoreCommons.getClassPathResource(NOT_FOUND_JSON_PATH, TestCommons.class).getFile().toPath()));
		EMPTY_COMMAND_OUTPUT.setBufferedOutput(new StringBuffer(empty_output));
	}

	public CommandOutput getCOMMAND_OUTPUT() {
		return COMMAND_OUTPUT;
	}

	public void setCOMMAND_OUTPUT(CommandOutput cOMMAND_OUTPUT) {
		COMMAND_OUTPUT = cOMMAND_OUTPUT;
	}

	public CommandOutput getEMPTY_COMMAND_OUTPUT() {
		return EMPTY_COMMAND_OUTPUT;
	}

	public void setEMPTY_COMMAND_OUTPUT(CommandOutput eMPTY_COMMAND_OUTPUT) {
		EMPTY_COMMAND_OUTPUT = eMPTY_COMMAND_OUTPUT;
	}
}
