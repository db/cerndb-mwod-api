package ch.cern.mwod.interfaces;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import ch.cern.mwod.command.CommandManager;
import ch.cern.mwod.config.DefaultsConfig;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InterfaceServicesTest {

	@Autowired
	private GetInterfaceService getInterfaceService;
	@Autowired
	private AddInterfaceService addInterfaceService;

	@MockBean
	private CommandManager commandManager;
	@MockBean
	private GetInterfaceCommand getInterfaceCommand;
	@MockBean
	private AddInterfaceCommand addInterfaceCommand;

	private static InterfaceTestsCommons testCommons;

	@BeforeClass
	public static void init() throws Exception {
		testCommons = new InterfaceTestsCommons(InterfaceIntegrationIT.class);
	}

	@Test
	public void testGetInterfaceService_when_sc_entity_and_sc_interface_id_exist() throws Exception {
		given(commandManager.execute(getInterfaceCommand)).willReturn(testCommons.getCOMMAND_OUTPUT());
		Interface result = getInterfaceService.get(testCommons.APACHE_SC_ENTITY, testCommons.getExpected_sc_interface_id(), true);
		assertThat(result.getScInterfaceGlobalId()).isEqualTo(testCommons.getIntfc().getScInterfaceGlobalId());
	}

	@Test
	public void testAddInterface_when_input_is_ok() throws Exception {
		given(commandManager.execute(addInterfaceCommand)).willReturn(testCommons.getCOMMAND_OUTPUT());
		String result = addInterfaceService.add(testCommons.APACHE_SC_ENTITY, testCommons.getIntfc(), true);
		assertThat(result).isEqualTo(testCommons.getExpectedInterfaceDN());
	}

}
