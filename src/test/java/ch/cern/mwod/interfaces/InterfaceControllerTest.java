package ch.cern.mwod.interfaces;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import ch.cern.mwod.config.DefaultsConfig;
import ch.cern.mwod.json.JsonManager;

@RunWith(SpringRunner.class)
@WebMvcTest(InterfaceController.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InterfaceControllerTest {

	@MockBean
	private GetInterfaceService getInterfaceService;
	@MockBean
	private AddInterfaceService addInterfaceService;
	@MockBean
	private DeleteInterfaceService deleteInterfaceService;
	
	@Autowired
	private MockMvc mockMvc;
	private static InterfaceTestsCommons testCommons;

	@BeforeClass
	public static void init() throws Exception {
		testCommons = new InterfaceTestsCommons(InterfaceServicesTest.class);
	}

	@Test
	public void test_A_get_Interface_when_sc_entity_and_sc_interface_id_exist() throws Exception {
		given(getInterfaceService.get(testCommons.APACHE_SC_ENTITY, testCommons.getExpected_sc_interface_id(), true))
				.willReturn(testCommons.getIntfc());
		this.mockMvc
				.perform(get("/entities/" + testCommons.APACHE_SC_ENTITY + "/interfaces/"
						+ testCommons.getEncoded_sc_interface_id()).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().json(JsonManager.deserializeJsonAsString(testCommons.getIntfc())));
	}

	@Test
	public void test_B_add_Interface_when_input_is_ok() throws Exception {
		given(addInterfaceService.add(testCommons.APACHE_SC_ENTITY, testCommons.getIntfc(), true))
				.willReturn(testCommons.getExpectedInterfaceDN());
		this.mockMvc
				.perform(post("/entities/" + testCommons.APACHE_SC_ENTITY + "/interfaces")
						.contentType(MediaType.APPLICATION_JSON)
						.content(JsonManager.deserializeJsonAsString(testCommons.getIntfc())))
				.andExpect(status().isCreated()).andExpect(content().string(testCommons.getExpectedInterfaceDN()));
	}
}
