package ch.cern.mwod.interfaces;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InterfaceIntegrationIT {

	@Autowired
	private TestRestTemplate testRestTemplate;
	private static InterfaceTestsCommons testCommons;
	
	@BeforeClass
	public static void init() throws Exception {
		testCommons = new InterfaceTestsCommons(InterfaceIntegrationIT.class);
	}

	@Test
	public void test_A_addInterface_when_input_is_ok() throws Exception {
		ResponseEntity<String> result = testCommons.addInterface(testRestTemplate);
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		assertThat(result.getBody()).isEqualTo(testCommons.getExpectedInterfaceDN());
	}

	@Test
	public void test_B_get_Interface_when_sc_entity_and_sc_interface_id_exist() throws Exception {
		ResponseEntity<Interface> result = this.testRestTemplate.getForEntity(testCommons.getGet_or_delete_url(), Interface.class);
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(result.getBody().getScInterfaceId()).isEqualTo(testCommons.getExpected_sc_interface_id());
	}
	
	@Test
	public void test_C_deleteInterface_when_sc_entity_and_interface_id_exist() throws Exception {
		ResponseEntity<String> result = testCommons.deleteInterface(testRestTemplate);
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(result.getBody()).isEqualTo(testCommons.getExpectedInterfaceDN());
	}
}
