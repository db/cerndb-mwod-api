package ch.cern.mwod.interfaces;

import java.nio.file.Files;
import java.util.Arrays;
import java.util.Base64;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import ch.cern.mwod.TestCommons;
import ch.cern.mwod.commandline.CommandOutput;
import ch.cern.mwod.commons.CoreCommons;
import ch.cern.mwod.json.JsonManager;

public class InterfaceTestsCommons extends TestCommons {

	public final String TOMCAT_JSON_PATH = "output_for_tomcat_docker_guys.json";
	public final String INTERFACE_OUTPUT_JSON_PATH = "interface_output.json";
	public final String INTERFACE_INPUT_JSON_PATH = "interface_input.json";
	public String interfacesBaseDN;
	private Interface intfc;
	private String expectedInterfaceDN;
	private String expected_sc_interface_id;
	private String encoded_sc_interface_id;
	private String get_or_delete_url;

	public InterfaceTestsCommons(Class<?> cl) throws Exception {
		super(cl);
		if(System.getenv("INTERFACES_BASE_DN")!=null && !System.getenv("INTERFACES_BASE_DN").isEmpty()){
			interfacesBaseDN = "SC-CATEGORY=interfaces,SC-ENTITY=$sc_entity,SC-CATEGORY=entities,ou=syscontrol,dc=cern,dc=ch";
		}
		COMMAND_OUTPUT = new CommandOutput();
		COMMAND_OUTPUT.setResult(0);
		String output = new String();
		output = new String(Files
				.readAllBytes(CoreCommons.getClassPathResource(INTERFACE_OUTPUT_JSON_PATH, cl).getFile().toPath()));
		COMMAND_OUTPUT.setBufferedOutput(new StringBuffer(output));
		Interface[] interfaces = (Interface[]) JsonManager.deserializeJson(
				CoreCommons.getClassPathResource(INTERFACE_OUTPUT_JSON_PATH, InterfaceServicesTest.class).getFile(),
				Interface[].class);
		intfc = interfaces[0];
		expected_sc_interface_id = intfc.getScInterfaceDomain() + "_" + RandomStringUtils.random(10, true, true);
		intfc.setScInterfaceId(expected_sc_interface_id);
		expectedInterfaceDN = "SC-INTERFACE-ID=" + expected_sc_interface_id + ","
				+ interfacesBaseDN.replace("$sc_entity", APACHE_SC_ENTITY);
		encoded_sc_interface_id = new String(Base64.getEncoder().encode(expected_sc_interface_id.getBytes()));
		get_or_delete_url = "/entities/" + APACHE_SC_ENTITY + "/interfaces/" + encoded_sc_interface_id;
	}

	public ResponseEntity<String> addInterface(TestRestTemplate testRestTemplate) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.TEXT_PLAIN));
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Interface> requestEntity = new HttpEntity<Interface>(getIntfc(), headers);
		ResponseEntity<String> result = testRestTemplate.postForEntity("/entities/" + APACHE_SC_ENTITY + "/interfaces",
				requestEntity, String.class);
		return result;
	}
	
	public ResponseEntity<String> deleteInterface(TestRestTemplate testRestTemplate) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.TEXT_PLAIN));
		HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
		ResponseEntity<String> result = testRestTemplate.exchange(getGet_or_delete_url(), HttpMethod.DELETE, requestEntity, String.class);
		return result;
	}

	public String getInterfacesBaseDN() {
		return interfacesBaseDN;
	}

	public void setInterfacesBaseDN(String interfacesBaseDN) {
		this.interfacesBaseDN = interfacesBaseDN;
	}

	public String getExpectedInterfaceDN() {
		return expectedInterfaceDN;
	}

	public void setExpectedInterfaceDN(String expectedInterfaceDN) {
		this.expectedInterfaceDN = expectedInterfaceDN;
	}

	public String getExpected_sc_interface_id() {
		return expected_sc_interface_id;
	}

	public void setExpected_sc_interface_id(String expected_sc_interface_id) {
		this.expected_sc_interface_id = expected_sc_interface_id;
	}

	public String getEncoded_sc_interface_id() {
		return encoded_sc_interface_id;
	}

	public void setEncoded_sc_interface_id(String encoded_sc_interface_id) {
		this.encoded_sc_interface_id = encoded_sc_interface_id;
	}

	public String getGet_or_delete_url() {
		return get_or_delete_url;
	}

	public void setGet_or_delete_url(String get_or_delete_url) {
		this.get_or_delete_url = get_or_delete_url;
	}

	public Interface getIntfc() {
		return intfc;
	}

	public void setIntfc(Interface intfc) {
		this.intfc = intfc;
	}
}
