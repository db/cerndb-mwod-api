#!/bin/sh

# setup the environment: JAVA_HOME and enviroment variables
./setup.sh
return_code=$?
if [ $return_code != 0 ] ; then
   echo "ERROR: setup.sh returns $return_code"
   exit 1
else
   echo "ENVIROMENT SETUP OK"
fi

# Remove target folder (binaries)
/home/lurodrig/development/ci/apache-maven-3.3.9/bin/mvn clean 

# Compile: create target folder 
/home/lurodrig/development/ci/apache-maven-3.3.9/bin/mvn compile

# Package: create the .war inside the target folder 
/home/lurodrig/development/ci/apache-maven-3.3.9/bin/mvn package -DskipUTs=true

# Build the container using the Dockerfile in the project root (current directory)
# This build will add the binaries (.war) and the scripts (vegas commands)
docker build -t gitlab-registry.cern.ch/db/cerndb-mwod-api:java8_maven3_mwod_rest_api . -f Dockerfile --force-rm

docker push gitlab-registry.cern.ch/db/cerndb-mwod-api

